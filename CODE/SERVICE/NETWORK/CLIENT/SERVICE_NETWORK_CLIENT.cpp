//
//  SERVICE_NETWORK_CLIENT.cpp
//  GAME-ENGINE-REBORN
//
//  Created by Christophe Bernard on 30/10/16.
//  Copyright © 2016 Christophe Bernard. All rights reserved.
//

#include "SERVICE_NETWORK_CLIENT.h"
#include "SERVICE_NETWORK_CONNECTION.h"

SERVICE_NETWORK_CLIENT::SERVICE_NETWORK_CLIENT() :
    UDPBroadcastConnection( NULL ),
    UDPListenConnection( NULL ),
    TCPConnection( NULL ),
    OnTCPConnectionLostCallback(),
    OnTCPConnectionResumedCallback(),
    OnUdpBroadcastMessageReceivedCallback(),
    TcpClientCommunicationThread(),
    TcpCommunicationTask( NULL ) {
    
}

SERVICE_NETWORK_CLIENT::~SERVICE_NETWORK_CLIENT() {
    
}

void SERVICE_NETWORK_CLIENT::Initialize() {
    
    SERVICE_NETWORK_SYSTEM::GetInstance().OnTCPDataReceivedCallback = new CORE_HELPERS_CALLBACK_2< SERVICE_NETWORK_COMMAND *, uv_stream_t * >( Wrapper2< SERVICE_NETWORK_CLIENT, SERVICE_NETWORK_COMMAND *, uv_stream_t *, &SERVICE_NETWORK_CLIENT::OnTCPDataReceived >, this );
    
    
    SERVICE_NETWORK_CONNECTION::OnNewConnexionCallback = CORE_HELPERS_CALLBACK_1<uv_connect_t*>(Wrapper1<SERVICE_NETWORK_CLIENT, uv_connect_t*, &SERVICE_NETWORK_CLIENT::OnTcpConnected>, this );
}

void SERVICE_NETWORK_CLIENT::OnTcpConnected( uv_connect_t* connexion_data) {
    
    TCPConnection->UVConnection.TCPType.TCPServer = connexion_data->handle;
}

void SERVICE_NETWORK_CLIENT::Finalize() {
    if ( UDPListenConnection ) {
        
        UDPListenConnection->Stop();
        
        delete UDPListenConnection;
        
        UDPListenConnection = NULL;
    }
}

void SERVICE_NETWORK_CLIENT::Update(const float time_step) {
    
}

void SERVICE_NETWORK_CLIENT::StartUDPListen() {
    
    SERVICE_NETWORK_SYSTEM::GetInstance().OnUPDDataReceivedCallback = new CORE_HELPERS_CALLBACK_1< SERVICE_NETWORK_COMMAND * >( Wrapper1< SERVICE_NETWORK_CLIENT, SERVICE_NETWORK_COMMAND *, &SERVICE_NETWORK_CLIENT::OnUDPDataReceived >, this );
    
    UDPListenConnection = SERVICE_NETWORK_SYSTEM::GetInstance().CreateConnection(
        SERVICE_NETWORK_CONNECTION_TYPE_Udp,
        SERVICE_NETWORK_SYSTEM::AllInterfaces,
        SERVICE_NETWORK_SYSTEM::AllInterfaces,
        1338,
        SERVICE_NETWORK_SYSTEM::BroadcastPortDefault,
        true,
        false );
    
    UDPListenConnection->Start();
    
    ListenCallback = new CORE_HELPERS_CALLBACK( &Wrapper< SERVICE_NETWORK_CLIENT, &SERVICE_NETWORK_CLIENT::Listen >, this );
}

void SERVICE_NETWORK_CLIENT::StopUDPListen() {
    
    UDPListenConnection->Stop();
}

void SERVICE_NETWORK_CLIENT::OnTCPDataReceived( SERVICE_NETWORK_COMMAND * command, uv_stream_t * tcp_stream) {
    
    (*OnTCPNetworkCommandReceivedCallback)( command );
}

void SERVICE_NETWORK_CLIENT::OnUDPDataReceived( SERVICE_NETWORK_COMMAND * command ) {

    OnUDPNetworkCommandReceivedCallback->operator()(command);
}

void SERVICE_NETWORK_CLIENT::Listen() {
    
    while ( true ) {
        
        SERVICE_NETWORK_SYSTEM::GetInstance().Update( true );
    }
}


void SERVICE_NETWORK_CLIENT::OnServerFound() {
    
    StopUDPListen();
}

void SERVICE_NETWORK_CLIENT::SendTcpCommand( CORE_DATA_STREAM & data_to_send, SERVICE_NETWORK_CONNECTION * connexion ) {
    
    TCPConnection->Send( data_to_send );
    //data_to_send.Close();
}
