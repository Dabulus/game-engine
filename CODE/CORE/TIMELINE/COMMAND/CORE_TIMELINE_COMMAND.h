//
//  CORE_TIMELINE_COMMAND.hpp
//  GAME-ENGINE-REBORN
//
//  Created by Christophe Bernard on 8/02/16.
//  Copyright © 2016 Christophe Bernard. All rights reserved.
//

#ifndef CORE_TIMELINE_COMMAND_h
#define CORE_TIMELINE_COMMAND_h

#include "CORE_HELPERS_CLASS.h"
#include "CORE_HELPERS_UNIQUE_IDENTIFIER.h"
#include "CORE_HELPERS_CALLBACK.h"

class CORE_TIMELINE_COMMAND  {

public :
    
CORE_TIMELINE_COMMAND();

virtual void Apply() = 0;

};

#endif /* CORE_TIMELINE_COMMAND_hpp */
