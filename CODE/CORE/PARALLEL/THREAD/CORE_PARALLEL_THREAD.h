//
//  CORE_PARALLEL_THREAD.h
//  GAME-ENGINE-REBORN
//
//  Created by Christophe Bernard on 31/08/15.
//  Copyright (c) 2015 Christophe Bernard. All rights reserved.
//

#ifndef __GAME_ENGINE_REBORN__CORE_PARALLEL_THREAD__
#define __GAME_ENGINE_REBORN__CORE_PARALLEL_THREAD__

#include "CORE_HELPERS_CLASS.h"
#include "CORE_PARALLEL_TASK.h"
#include "CORE_RUNTIME_ENVIRONMENT.h"

#if PLATFORM_WINDOWS
	#define __WINDOWS_THREAD__ 1
#else
	#define __P__THREAD__ 1

    #include <pthread.h>
#endif

XS_CLASS_BEGIN( CORE_PARALLEL_THREAD )

CORE_PARALLEL_THREAD();

void Initialize( const char * thread_name, CORE_PARALLEL_TASK & task );

void Start();

void Stop();
void Pause();
void Resume();

CORE_PARALLEL_TASK
    * Task;
char
    * ThreadName;

static const char * DefaultThreadName;

#if __P__THREAD__
    #include "CORE_PARALLEL_THREAD_POSIX.hpp"
#else
    #include "CORE_PARALLEL_THREAD_WINDOWS.hpp"
#endif

XS_CLASS_END


#endif /* defined(__GAME_ENGINE_REBORN__CORE_PARALLEL_THREAD__) */
