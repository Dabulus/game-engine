//
//  CORE_MATH_SHAPE_TYPE.cpp
//  GAME-ENGINE-REBORN
//
//  Created by Christophe Bernard on 21/02/16.
//  Copyright © 2016 Christophe Bernard. All rights reserved.
//

#include "CORE_MATH_SHAPE_TYPE.h"
#include "CORE_HELPERS_CLASS.h"
#include "CORE_DATA_STREAM.h"

XS_CLASS_SERIALIZER_TemplateScalar( CORE_MATH_SHAPE_TYPE )