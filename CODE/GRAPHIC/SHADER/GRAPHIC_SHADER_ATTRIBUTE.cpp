//
//  GRAPHIC_SHADER_ATTRIBUTE.cpp
//  GAME-ENGINE-REBORN
//
//  Created by Christophe Bernard on 2/05/14.
//  Copyright (c) 2014 Christophe Bernard. All rights reserved.
//

#include "GRAPHIC_SHADER_ATTRIBUTE.h"

GRAPHIC_SHADER_ATTRIBUTE::GRAPHIC_SHADER_ATTRIBUTE() :
    AttributeName( CORE_HELPERS_IDENTIFIER::Empty ),
    AttributeValue(),
    AttributeIndex( 0 ) {
    
}

GRAPHIC_SHADER_ATTRIBUTE::~GRAPHIC_SHADER_ATTRIBUTE() {

}