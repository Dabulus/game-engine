//
//  GRAPHIC_UI_SYSTEM.cpp
//  GAME-ENGINE-REBORN
//
//  Created by Christophe Bernard on 20/12/15.
//  Copyright © 2015 Christophe Bernard. All rights reserved.
//

#include "GRAPHIC_UI_SYSTEM.h"
#include "CORE_ABSTRACT_PROGRAM_LUA.h"
#include "GRAPHIC_UI_ELEMENT_SCRIPTED.h"

CORE_ABSTRACT_PROGRAM_BINDER_DECLARE_CLASS( GRAPHIC_UI_SYSTEM )
    CORE_ABSTRACT_PROGRAM_BINDER_DEFINE_YIELD_METHOD( GRAPHIC_UI_FRAME *, GRAPHIC_UI_SYSTEM, GetCurrentScreen )
    CORE_ABSTRACT_PROGRAM_BINDER_DEFINE_STATIC_YIELD_METHOD( GRAPHIC_UI_SYSTEM &, GRAPHIC_UI_SYSTEM, GetInstance )
CORE_ABSTRACT_PROGRAM_BINDER_END_CLASS( GRAPHIC_UI_SYSTEM )

GRAPHIC_UI_SYSTEM::GRAPHIC_UI_SYSTEM() :
    ScreenTable(),
    CurrentScreen( NULL ) {
    
}

GRAPHIC_UI_SYSTEM::~GRAPHIC_UI_SYSTEM() {

}

void GRAPHIC_UI_SYSTEM::Update( float time_step ) {
    
    std::map<CORE_HELPERS_IDENTIFIER, GRAPHIC_UI_FRAME * >::iterator it;
    
    it = ScreenTable.begin();
    
    while ( it != ScreenTable.end()) {
        
        if ( it->second->IsEnabled() ) {
            
            it->second->Update( time_step );
        }
        
        it++;
    }
}

void GRAPHIC_UI_SYSTEM::Render() {
    
    std::map<CORE_HELPERS_IDENTIFIER, GRAPHIC_UI_FRAME * >::iterator it;
    
    it = ScreenTable.begin();
    
    while ( it != ScreenTable.end()) {
        
        if ( it->second->IsVisible() ) {
            
            it->second->Render( GRAPHIC_RENDERER::GetInstance() );
        }
        
        it++;
    }
}

void GRAPHIC_UI_SYSTEM::RegisterScreen( GRAPHIC_UI_FRAME * screen, const char * screen_name ) {
    
    ScreenTable[ CORE_HELPERS_IDENTIFIER( screen_name ) ] = screen;
    
    if ( CurrentScreen == NULL ) {
        
        CurrentScreen = screen;
    }
}

void GRAPHIC_UI_SYSTEM::RegisterScritpedScreen( const CORE_FILESYSTEM_PATH & script_path, const char * screen_name ) {
    
    GRAPHIC_UI_FRAME * frame = new GRAPHIC_UI_FRAME;
    
    GRAPHIC_UI_ELEMENT_SCRIPTED * script_element = new GRAPHIC_UI_ELEMENT_SCRIPTED;
    
    frame->SetIdentifier(CORE_HELPERS_IDENTIFIER( screen_name ));
    
    script_element->InitializeFrame( script_path, frame );
    
    ScreenTable[ frame->GetIdentifier() ] = frame;
}