    //
//  GRAPHIC_UI_FRAME.cpp
//  GAME-ENGINE-REBORN
//
//  Created by Christophe Bernard on 16/06/15.
//  Copyright (c) 2015 Christophe Bernard. All rights reserved.
//

#include "GRAPHIC_UI_FRAME.h"
#include "GRAPHIC_UI_FRAME_ADAPTER.h"

CORE_ABSTRACT_PROGRAM_BINDER_DECLARE_CLASS( GRAPHIC_UI_FRAME )
    CORE_ABSTRACT_PROGRAM_BINDER_DEFINE_YIELD_METHOD_1( GRAPHIC_UI_ELEMENT *, GRAPHIC_UI_FRAME, Contains, CORE_MATH_VECTOR & )
    CORE_ABSTRACT_PROGRAM_BINDER_DEFINE_YIELD_METHOD_1( GRAPHIC_UI_ELEMENT *, GRAPHIC_UI_FRAME, GetElement, const char * )
CORE_ABSTRACT_PROGRAM_BINDER_DEFINE_VOID_METHOD_1(GRAPHIC_UI_FRAME, AddObject, GRAPHIC_UI_ELEMENT * )
CORE_ABSTRACT_PROGRAM_BINDER_END_CLASS( GRAPHIC_UI_FRAME )

GRAPHIC_UI_FRAME::GRAPHIC_UI_FRAME() :
    GRAPHIC_UI_ELEMENT(),
    ElementTable() {
    
}

GRAPHIC_UI_FRAME::~GRAPHIC_UI_FRAME() {
    
    std::map<CORE_HELPERS_IDENTIFIER, GRAPHIC_UI_ELEMENT *>::iterator it = ElementTable.begin();
    
    do {
        
        delete it->second;
        
        it++;
        
    } while ( it != ElementTable.end() );
}

GRAPHIC_UI_ELEMENT * GRAPHIC_UI_FRAME::Contains( CORE_MATH_VECTOR & cursor_position ) {
    
    if ( GRAPHIC_UI_ELEMENT::Contains( cursor_position ) ) {
        
        std::map<CORE_HELPERS_IDENTIFIER, GRAPHIC_UI_ELEMENT *>::iterator it = ElementTable.begin();
        
        do {
            
            GRAPHIC_UI_ELEMENT * element = it->second->Contains( cursor_position );
            
            if( element ) {
                
                return element;
            }
            
            it++;
            
        } while ( it != ElementTable.end() );
    }
    
    return NULL;
}

void GRAPHIC_UI_FRAME::Update( const float time_step ) {
    
    if ( IsEnabled() ) {
        
        GRAPHIC_UI_ELEMENT::Update( time_step );
        
        std::map<CORE_HELPERS_IDENTIFIER, GRAPHIC_UI_ELEMENT *>::iterator it = ElementTable.begin();
        
        do {
            
            it->second->Update( time_step );
            
            it++;
        } while ( it != ElementTable.end() );
    }
}

void GRAPHIC_UI_FRAME::Render( const GRAPHIC_RENDERER & renderer ) {
    
    if ( IsVisible() ) {
        
        std::map<CORE_HELPERS_IDENTIFIER, GRAPHIC_UI_ELEMENT *>::iterator it = ElementTable.begin();
        
        do {
            
            it->second->Render( renderer );
            
            it++;
            
        } while ( it != ElementTable.end() );
    }
}

void GRAPHIC_UI_FRAME::Click( CORE_MATH_VECTOR & cursor_position ) {
    
    if ( Contains( cursor_position ) ) {
        
        std::map<CORE_HELPERS_IDENTIFIER, GRAPHIC_UI_ELEMENT *>::iterator it = ElementTable.begin();
        
        do {
            
            GRAPHIC_UI_ELEMENT * element = it->second->Contains( cursor_position );
            
            if( element != NULL && element->IsEnabled() && element->IsVisible() ) {
                
                if ( element->GetCurrentState() != GRAPHIC_UI_ELEMENT_STATE_Pressed ) {
                    
                    element->Click( cursor_position );
                }
                
                return;
            }
            
            it++;
            
        } while ( it != ElementTable.end() );
        
        ActionCallback( this, CurrentState );
    }
}

void GRAPHIC_UI_FRAME::Hover( const CORE_MATH_VECTOR & cursor_position ) {
    
    if ( GRAPHIC_UI_ELEMENT::Contains( cursor_position ) ) {
        
        std::map<CORE_HELPERS_IDENTIFIER, GRAPHIC_UI_ELEMENT *>::iterator it = ElementTable.begin();
        
        do {
            
            it->second->Hover( cursor_position );
            
            it++;
            
        } while ( it != ElementTable.end() );
        
        ActionCallback( this, CurrentState );
    }
}

GRAPHIC_UI_ELEMENT * GRAPHIC_UI_FRAME::GetObjectForIdentifier( const CORE_HELPERS_IDENTIFIER & identifier ) {
    
    if ( Identifier == identifier ) {
        
        return this;
    }
    
    std::map<CORE_HELPERS_IDENTIFIER, GRAPHIC_UI_ELEMENT *>::iterator it = ElementTable.begin();
    
    do {
        
        if( it->first == identifier ) {
            
            return it->second;
        }
        
        it++;
        
    } while ( it != ElementTable.end() );
    
    return NULL;
}

void GRAPHIC_UI_FRAME::SetObjectForIdentifier( const CORE_HELPERS_IDENTIFIER & identifier, GRAPHIC_UI_ELEMENT * element ) {
    
    ElementTable[ identifier ] = element;
    
    ((GRAPHIC_UI_FRAME_ADAPTER *) Adapter)->OnLayoutItems( this );
}

void GRAPHIC_UI_FRAME::AddObject( GRAPHIC_UI_ELEMENT * element ) {
    
    ElementTable[ element->GetIdentifier() ] = element;
    
    if ( Adapter ) {
        
        ((GRAPHIC_UI_FRAME_ADAPTER *) Adapter)->OnLayoutItems( this );
    }
}

GRAPHIC_UI_ELEMENT * GRAPHIC_UI_FRAME::GetElement( const char * element_name ) {
    
    CORE_HELPERS_IDENTIFIER identifier ( element_name );
    
    std::map<CORE_HELPERS_IDENTIFIER, GRAPHIC_UI_ELEMENT *>::iterator it = ElementTable.begin();
    
    do {
        
        if( it->first == identifier ) {
            
            return it->second;
        }
        
        it++;
        
    } while ( it != ElementTable.end() );
    
    return NULL;
}

void GRAPHIC_UI_FRAME::OnPlacementPropertyChanged() {

    std::map<CORE_HELPERS_IDENTIFIER, GRAPHIC_UI_ELEMENT *>::iterator it = ElementTable.begin();
    
    do {
        
        it->second->GetPlacement().OnPlacementPropertyChanged();
        
        it++;
    } while ( it != ElementTable.end() );
}

GRAPHIC_UI_ELEMENT * GRAPHIC_UI_FRAME::Copy() {
    
    GRAPHIC_UI_FRAME * otherFrame = new GRAPHIC_UI_FRAME();
    
    std::map<CORE_HELPERS_IDENTIFIER, GRAPHIC_UI_ELEMENT *>::iterator it = ElementTable.begin();
    
    do {
        
        otherFrame->ElementTable[it->first] = it->second->Copy();
        
        it++;
    } while ( it != ElementTable.end() );
    
    return otherFrame;
}
