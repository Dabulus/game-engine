//
//  GRAPHIC_UI_FRAME_LIST_ADAPTER.hpp
//  GAME-ENGINE-REBORN
//
//  Created by Christophe Bernard on 21/11/16.
//  Copyright © 2016 Christophe Bernard. All rights reserved.
//

#ifndef GRAPHIC_UI_FRAME_LIST_ADAPTER_hpp
#define GRAPHIC_UI_FRAME_LIST_ADAPTER_hpp

#include "CORE_HELPERS_CLASS.h"
#include "GRAPHIC_UI_BASE_ADAPTER.h"
#include "GRAPHIC_UI_FRAME_ADAPTER.h"

XS_CLASS_BEGIN_WITH_ANCESTOR( GRAPHIC_UI_FRAME_LIST_ADAPTER, GRAPHIC_UI_FRAME_ADAPTER )

GRAPHIC_UI_FRAME_LIST_ADAPTER();
GRAPHIC_UI_FRAME_LIST_ADAPTER( GRAPHIC_UI_ELEMENT * ui_template );

virtual void OnResize( GRAPHIC_UI_ELEMENT * );
virtual void OnLayoutItems( GRAPHIC_UI_FRAME * );

void GetItemsCount( int count );


private:

GRAPHIC_UI_ELEMENT
    * UITemplate;

XS_CLASS_END


#endif /* GRAPHIC_UI_FRAME_LIST_ADAPTER_hpp */
