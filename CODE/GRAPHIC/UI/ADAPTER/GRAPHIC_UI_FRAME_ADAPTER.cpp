//
//  GRAPHIC_UI_FRAME_ADAPTER.cpp
//  GAME-ENGINE-REBORN
//
//  Created by Christophe Bernard on 6/02/16.
//  Copyright © 2016 Christophe Bernard. All rights reserved.
//

#include "GRAPHIC_UI_FRAME_ADAPTER.h"
#include "GRAPHIC_UI_FRAME.h"

GRAPHIC_UI_FRAME_ADAPTER::GRAPHIC_UI_FRAME_ADAPTER() :
    GRAPHIC_UI_BASE_ADAPTER() {
    
}

GRAPHIC_UI_FRAME_ADAPTER::~GRAPHIC_UI_FRAME_ADAPTER() {
    
}

void GRAPHIC_UI_FRAME_ADAPTER::OnLayoutItems( GRAPHIC_UI_FRAME * frame ) {
    
    
}

void GRAPHIC_UI_FRAME_ADAPTER::OnResize( GRAPHIC_UI_ELEMENT * ) {
    
}

void GRAPHIC_UI_FRAME_ADAPTER::OnMove( GRAPHIC_UI_ELEMENT * ) {
    
}