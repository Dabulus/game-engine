//
//  GRAPHIC_UI_RENDER_STYLE.cpp
//  GAME-ENGINE-REBORN
//
//  Created by Christophe Bernard on 16/06/15.
//  Copyright (c) 2015 Christophe Bernard. All rights reserved.
//

#include "GRAPHIC_UI_RENDER_STYLE.h"
#include "GRAPHIC_SHADER_PROGRAM.h"
#include "GRAPHIC_SHADER_ATTRIBUTE.h"

GRAPHIC_UI_RENDER_STYLE::GRAPHIC_UI_RENDER_STYLE() :
    Color( 0.0f, 0.0f, 0.0f, 1.0f),
    Shape(),
    TextureBlock() {
    
}

GRAPHIC_UI_RENDER_STYLE::~GRAPHIC_UI_RENDER_STYLE() {

}

void GRAPHIC_UI_RENDER_STYLE::Apply( const GRAPHIC_RENDERER & renderer, const GRAPHIC_UI_PLACEMENT & placement ) {
    
    if ( Shape && TextureBlock ) {
        
        Shape->SetPosition( placement.GetAbsolutePosition() );
        Shape->SetScaleFactor( placement.GetSize() );
        Shape->SetTextureBlock( TextureBlock );
        GRAPHIC_SHADER_ATTRIBUTE & color_attribute = Shape->GetShaderTable()[0]->getShaderAttribute( GRAPHIC_SHADER_PROGRAM::GeometryColor );
        
        color_attribute.AttributeValue.Value.FloatArray4[0] = Color[0];
        color_attribute.AttributeValue.Value.FloatArray4[1] = Color[1];
        color_attribute.AttributeValue.Value.FloatArray4[2] = Color[2];
        color_attribute.AttributeValue.Value.FloatArray4[3] = Color[3];
        
        Shape->Render( renderer );
    }
}