//
//  GRAPHIC_OBJECT_SHAPE_GRID.cpp
//  GAME-ENGINE-REBORN
//
//  Created by Christophe Bernard on 1/05/15.
//  Copyright (c) 2015 Christophe Bernard. All rights reserved.
//

#include "GRAPHIC_OBJECT_SHAPE_GRID.h"

GRAPHIC_OBJECT_SHAPE_GRID::~GRAPHIC_OBJECT_SHAPE_GRID() {

}

void GRAPHIC_OBJECT_SHAPE_GRID::InitializeShape( GRAPHIC_SHADER_PROGRAM_DATA_PROXY::PTR shader ) {

}

void GRAPHIC_OBJECT_SHAPE_GRID::Render( const GRAPHIC_RENDERER & renderer ) {

}