//
//  GRAPHIC_RENDER_TARGET_OPENGLES2.h
//  GAME-ENGINE-REBORN
//
//  Created by Christophe Bernard on 14/09/15.
//  Copyright (c) 2015 Christophe Bernard. All rights reserved.
//

#ifndef GAME_ENGINE_REBORN_GRAPHIC_RENDER_TARGET_OPENGLES2_h
#define GAME_ENGINE_REBORN_GRAPHIC_RENDER_TARGET_OPENGLES2_h

GLuint
    FrameBuffer,
    DepthrenderBuffer;
GLenum
    DrawBuffers[1];

#endif
