//
//  GLOBAL_RESOURCES.hpp
//  GAME-ENGINE-REBORN
//
//  Created by Christophe Bernard on 4/12/16.
//  Copyright © 2016 Christophe Bernard. All rights reserved.
//

#ifndef GLOBAL_RESOURCES_hpp
#define GLOBAL_RESOURCES_hpp

#include "CORE_HELPERS_CLASS.h"
#include "GRAPHIC_TEXTURE_ATLAS.h"
#include "GRAPHIC_OBJECT_SHAPE_PLAN.h"

XS_CLASS_BEGIN( GLOBAL_RESOURCES )

    XS_DEFINE_UNIQUE( GLOBAL_RESOURCES )

    void Initialize();
    void Finalize();
    
    GRAPHIC_TEXTURE_ATLAS
        UITextureAtlas;
    GRAPHIC_OBJECT_SHAPE_PLAN
        * UIPlanObject;
};

#endif /* GLOBAL_RESOURCES_hpp */
