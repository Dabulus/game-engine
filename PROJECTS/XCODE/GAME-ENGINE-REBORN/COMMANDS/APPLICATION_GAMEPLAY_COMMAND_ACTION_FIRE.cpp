//
//  APPLICATION_GAMEPLAY_COMMAND_ACTION_FIRE.cpp
//  GAME-ENGINE-REBORN
//
//  Created by Christophe Bernard on 12/02/16.
//  Copyright © 2016 Christophe Bernard. All rights reserved.
//

#include "APPLICATION_COMMAND.h"

XS_IMPLEMENT_INTERNAL_MEMORY_LAYOUT( APPLICATION_GAMEPLAY_COMMAND_ACTION_FIRE )

XS_END_INTERNAL_MEMORY_LAYOUT