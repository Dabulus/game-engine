//
//  APPLICATION_NETWORK_COMMAND_CLIENT_CONNECTED.cpp
//  GAME-ENGINE-REBORN
//
//  Created by Christophe Bernard on 10/02/16.
//  Copyright © 2016 Christophe Bernard. All rights reserved.
//

#include "APPLICATION_COMMAND.h"

XS_IMPLEMENT_INTERNAL_MEMORY_LAYOUT( APPLICATION_NETWORK_COMMAND_CLIENT_CONNECTED )
    XS_DEFINE_ClassMember( CORE_HELPERS_UNIQUE_IDENTIFIER, ClientIdentifier )
    XS_DEFINE_ClassMemberArray( char, (char **) &ClientName, (int)strlen( ClientName ) )
XS_END_INTERNAL_MEMORY_LAYOUT