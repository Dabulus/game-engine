//
//  APPLICATION_COMMAND_MANAGER.hpp
//  GAME-ENGINE-REBORN
//
//  Created by Christophe Bernard on 1/11/16.
//  Copyright © 2016 Christophe Bernard. All rights reserved.
//

#ifndef APPLICATION_COMMAND_MANAGER_hpp
#define APPLICATION_COMMAND_MANAGER_hpp

#include "CORE_HELPERS_CLASS.h"
#include "CORE_HELPERS_UNIQUE.h"
#include "CORE_TIMELINE.h"
#include "CORE_MATH_VECTOR.h"
#include "GAMEPLAY_COMPONENT_ENTITY.h"
#include "CORE_DATA_STREAM.h"

XS_CLASS_BEGIN(APPLICATION_COMMAND_MANAGER)

XS_DEFINE_UNIQUE(APPLICATION_COMMAND_MANAGER)

void ProcessEvent( CORE_TIMELINE_EVENT * event );
void OnTimelineEventAdded( CORE_TIMELINE_EVENT * event );
void CommandMoveCube( const CORE_MATH_VECTOR & new_position, GAMEPLAY_COMPONENT_ENTITY * entity );
inline CORE_TIMELINE & GetTimeline() { return Timeline; }

private :

CORE_TIMELINE
    Timeline;
XS_CLASS_END

#endif /* APPLICATION_COMMAND_MANAGER_hpp */
