//
//  NETWORK_CLIENT.cpp
//  GAME-ENGINE-REBORN
//
//  Created by Christophe Bernard on 31/10/16.
//  Copyright © 2016 Christophe Bernard. All rights reserved.
//

#include "NETWORK_CLIENT.h"

//--State EVENT
CORE_FIXED_STATE_DefineStateEnterEvent( NETWORK_CLIENT::INITIAL_STATE )

CORE_FIXED_STATE_EndOfStateEvent()
//--END

    //--State EVENT
    CORE_FIXED_STATE_DefineStateEvent( NETWORK_CLIENT::INITIAL_STATE, UDP_RECEIVED_EVENT )

        SERVICE_NETWORK_COMMAND const * command = event.GetEventData();

        CORE_DATA_STREAM
            stream( ( char * ) command->Data, command->Size );

        char * txt = (char*) malloc(command->Size);
        unsigned int size;

        stream >> size;
        stream.OutputBytes( (char ** ) & txt, command->Size );

        stream.Close();

        SERVICE_LOGGER_Error( "Message : %s\n", txt );

        GetContext().OnServerStatusCallback.operator()(command);

    CORE_FIXED_STATE_EndOfStateEvent()

    CORE_FIXED_STATE_DefineStateEvent( NETWORK_CLIENT::INITIAL_STATE, UDP_SERVER_SELECTED_EVENT )

        GetContext().ClientInstance->OnTCPNetworkCommandReceivedCallback = new CORE_HELPERS_CALLBACK_1< SERVICE_NETWORK_COMMAND * >( &Wrapper1< NETWORK_CLIENT , SERVICE_NETWORK_COMMAND *, &NETWORK_CLIENT::OnTCPDataReceived >, this );

        /*if ( TcpCommunicationTask == NULL && ( command->Address[0] != 0 || command->Address[1] != 0 || command->Address[2] != 0 || command->Address[3] != 0 ) ) {
            
            OnServerFound();
            
            TCPConnection = SERVICE_NETWORK_SYSTEM::GetInstance().CreateConnection(
                                                                                   SERVICE_NETWORK_CONNECTION_TYPE_Tcp,
                                                                                   command->Address,
                                                                                   SERVICE_NETWORK_SYSTEM::AllInterfaces,
                                                                                   1339,
                                                                                   SERVICE_NETWORK_SYSTEM::BroadcastPortDefault,
                                                                                   false );
            
            TCPConnection->Start();
            
            ListenCallback = new CORE_HELPERS_CALLBACK( &Wrapper< SERVICE_NETWORK_CLIENT, &SERVICE_NETWORK_CLIENT::Listen >, this );
            
            TcpCommunicationTask =  new CORE_PARALLEL_TASK ( ListenCallback );
            
            TcpClientCommunicationThread.Initialize("TCP client thread", *TcpCommunicationTask );
            TcpClientCommunicationThread.Start();
            
            SERVICE_LOGGER_Error( "Network tcp connexion started \n" );
        }*/

    CORE_FIXED_STATE_EndOfStateEvent()
    //--END

    //--State EVENT
    CORE_FIXED_STATE_DefineStateEvent( NETWORK_CLIENT::INITIAL_STATE, QUIT_EVENT )

    CORE_FIXED_STATE_EndOfStateEvent()
    //--State EVENT

//--State EVENT
CORE_FIXED_STATE_DefineStateLeaveEvent( NETWORK_CLIENT::INITIAL_STATE )

CORE_FIXED_STATE_EndOfStateEvent()
//--State EVENT

CORE_FIXED_STATE_DefineStateEnterEvent( NETWORK_CLIENT::TCP_CHALLENGE_STATE )

CORE_FIXED_STATE_EndOfStateEvent()

    CORE_FIXED_STATE_DefineStateEvent( NETWORK_CLIENT::TCP_CHALLENGE_STATE, TCP_CONNECTED_EVENT )

    CORE_FIXED_STATE_EndOfStateEvent()

    CORE_FIXED_STATE_DefineStateEvent( NETWORK_CLIENT::TCP_CHALLENGE_STATE, SERVER_REJECTED_CONNECTION_EVENT )

    CORE_FIXED_STATE_EndOfStateEvent()

    CORE_FIXED_STATE_DefineStateEvent( NETWORK_CLIENT::TCP_CHALLENGE_STATE, QUIT_EVENT )

    CORE_FIXED_STATE_EndOfStateEvent()

CORE_FIXED_STATE_DefineStateLeaveEvent( NETWORK_CLIENT::TCP_CHALLENGE_STATE )

CORE_FIXED_STATE_EndOfStateEvent()

CORE_FIXED_STATE_DefineStateEnterEvent( NETWORK_CLIENT::CONNECTED_STATE )

CORE_FIXED_STATE_EndOfStateEvent()

    CORE_FIXED_STATE_DefineStateEvent( NETWORK_CLIENT::CONNECTED_STATE, SELF_QUIT_EVENT )

    CORE_FIXED_STATE_EndOfStateEvent()

    CORE_FIXED_STATE_DefineStateEvent( NETWORK_CLIENT::CONNECTED_STATE, SERVER_DISCONNECTED_EVENT )

    CORE_FIXED_STATE_EndOfStateEvent()

    CORE_FIXED_STATE_DefineStateEvent( NETWORK_CLIENT::CONNECTED_STATE, QUIT_EVENT )

    CORE_FIXED_STATE_EndOfStateEvent()

    CORE_FIXED_STATE_DefineStateEvent( NETWORK_CLIENT::CONNECTED_STATE, SERVER_KICK_EVENT )

    CORE_FIXED_STATE_EndOfStateEvent()

CORE_FIXED_STATE_DefineStateLeaveEvent( NETWORK_CLIENT::CONNECTED_STATE )

CORE_FIXED_STATE_EndOfStateEvent()



CORE_FIXED_STATE_DefineStateEnterEvent( NETWORK_CLIENT::NETWORK_RECOVER_STATE )

CORE_FIXED_STATE_EndOfStateEvent()

    CORE_FIXED_STATE_DefineStateEvent( NETWORK_CLIENT::NETWORK_RECOVER_STATE, SELF_QUIT_EVENT )

    CORE_FIXED_STATE_EndOfStateEvent()

    CORE_FIXED_STATE_DefineStateEvent( NETWORK_CLIENT::NETWORK_RECOVER_STATE, QUIT_EVENT )

    CORE_FIXED_STATE_EndOfStateEvent()

    CORE_FIXED_STATE_DefineStateEvent( NETWORK_CLIENT::NETWORK_RECOVER_STATE, NETWORK_RECOVERED_EVENT )

    CORE_FIXED_STATE_EndOfStateEvent()

CORE_FIXED_STATE_DefineStateLeaveEvent( NETWORK_CLIENT::NETWORK_RECOVER_STATE )

CORE_FIXED_STATE_EndOfStateEvent()



NETWORK_CLIENT::NETWORK_CLIENT() :
    ClientInstance( NULL ),
    IncommingMessageQueue(),
    IncommingMessageQueueIterator(0),
    NetworkEventsTimeLine(),
    CurrentPlayer( 0, true ),
    NetworkRefreshRate(0.1f),
    AccumulatedRemaining(0.0f){
    
}

NETWORK_CLIENT::~NETWORK_CLIENT() {
    
}

void NETWORK_CLIENT::Initialize() {

    CORE_FIXED_STATE_InitializeState( StateMachine, NETWORK_CLIENT::INITIAL_STATE, this );

    ClientInstance = new SERVICE_NETWORK_CLIENT();
    ClientInstance->Initialize();
    ClientInstance->StartUDPListen();
    
    ClientInstance->OnUDPNetworkCommandReceivedCallback = new CORE_HELPERS_CALLBACK_1< SERVICE_NETWORK_COMMAND * >( &Wrapper1< NETWORK_CLIENT , SERVICE_NETWORK_COMMAND *, &NETWORK_CLIENT::OnUDPDataReceived >, this );
}

void NETWORK_CLIENT::Update( float time_step ) {
    
    AccumulatedRemaining += time_step;
    
    ClientInstance->Update( time_step );
    
    if ( NetworkRefreshRate > 0.0f ) {
        
        if ( AccumulatedRemaining > NetworkRefreshRate ) {
            
            AccumulatedRemaining -=NetworkRefreshRate;
            
            ProcessIncommingMessages();
            ProcessAndSendOutgoingMessages();
        }
    }
    else
    {
        ProcessIncommingMessages();
        ProcessAndSendOutgoingMessages();
    }
}

void NETWORK_CLIENT::Finalize() {
    
}

void NETWORK_CLIENT::OnTCPDataReceived( SERVICE_NETWORK_COMMAND * command ) {
    
    //TODO: check race condition
    IncommingMessageQueue[IncommingMessageQueueIterator++] = command;
    
    if ( IncommingMessageQueueIterator == THIS_GAME_MAX_NETWORK_MESSAG_QUEUE_SIZE ) {
        IncommingMessageQueueIterator = 0;
    }
}

void NETWORK_CLIENT::OnUDPDataReceived( SERVICE_NETWORK_COMMAND * command ) {
    
    StateMachine.DispatchEvent(UDP_RECEIVED_EVENT(command));
}

void NETWORK_CLIENT::DispatchMessageToAllPlayers(CORE_DATA_STREAM & data_buffer) {
    
    SERVICE_NETWORK_COMMAND * message = new SERVICE_NETWORK_COMMAND();
    message->Data = malloc(sizeof(data_buffer.GetAllocatedBytes()));
    message->Size = data_buffer.GetAllocatedBytes();
    
    memcpy(message->Data, data_buffer.GetMemoryBuffer(),data_buffer.GetAllocatedBytes());
    CurrentPlayer.AppendMessage(message);
}

void NETWORK_CLIENT::DispatchMessageToPlayer(NETWORK_PLAYER & player, CORE_DATA_STREAM & data_buffer) {
    
}

void NETWORK_CLIENT::Disconnect() {
    
}

void NETWORK_CLIENT::ProcessIncommingMessages() {
    
    SERVICE_NETWORK_COMMAND * command = NULL;
    CORE_TIMELINE_EVENT * event = NULL;
    
    for (int i = 0; i < IncommingMessageQueue.size(); i++ ) {
        
        command = IncommingMessageQueue[i];
        
        if ( command == NULL ) {
            continue;
        }
        
        CORE_DATA_STREAM
        stream( (char *) command->Data, command->Size );
        
        CORE_RUNTIME_Abort();
        //stream.Open();
        //stream.OutputBytes(event, command->Size);
        
        stream.Close();
        
        APPLICATION_COMMAND_MANAGER::GetInstance().ProcessEvent( event );
        
        delete IncommingMessageQueue[i];
        IncommingMessageQueue[i] = NULL;
    }
}

void NETWORK_CLIENT::ProcessAndSendOutgoingMessages() {
    
    ClientInstance->SendTcpCommand(
        CurrentPlayer.PrepareMessage(),
        CurrentPlayer.GetNetworkConnexion() );
}
