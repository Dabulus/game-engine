//
//  NETWORK_SERVER.cpp
//  GAME-ENGINE-REBORN
//
//  Created by Christophe Bernard on 29/10/16.
//  Copyright © 2016 Christophe Bernard. All rights reserved.
//

#include "NETWORK_SERVER.h"
#include "CORE_MATH_VECTOR.h"

NETWORK_SERVER::NETWORK_SERVER() :
    LobbyInstance( NULL ),
    PlayerTable(),
    IncommingMessageQueue(),
    IncommingMessageQueueIterator( 0 ),
    NetworkRefreshRate( 0.0f ),
    AccumulatedRemaining( 0.0f ),
    NetworkEventsTimeLine() {
    
    for (int i = 0; i < THIS_GAME_MAX_NETWORK_PLAYER_SIZE; i++ ) {
        
        PlayerTable[i] = NULL;
    }
     
    for (int i = 0; i < THIS_GAME_MAX_NETWORK_MESSAG_QUEUE_SIZE; i++ ) {

        IncommingMessageQueue[i] = NULL;
    }
}

NETWORK_SERVER::~NETWORK_SERVER() {
    
}

void NETWORK_SERVER::Initialize( float network_refresh_rate ) {
    
    LobbyInstance = new SERVICE_NETWORK_LOBBY();
    NetworkRefreshRate = network_refresh_rate;
    
    LobbyInstance->Initialize(THIS_GAME_MAX_NETWORK_PLAYER_SIZE, "XS_SERVER_ACCEPTS_CONNECTIONS" );
    
    LobbyInstance->OnTCPNetworkCommandReceivedCallback = new CORE_HELPERS_CALLBACK_1< SERVICE_NETWORK_COMMAND * >( &Wrapper1< NETWORK_SERVER , SERVICE_NETWORK_COMMAND *, &NETWORK_SERVER::OnTCPDataReceived >, this );
    
    LobbyInstance->OnTCPNewConnectionCallback = CORE_HELPERS_CALLBACK_1< SERVICE_NETWORK_CONNECTION * >( &Wrapper1< NETWORK_SERVER, SERVICE_NETWORK_CONNECTION * , &NETWORK_SERVER::OnLobbyTCPNewConnection>, this );
}

void NETWORK_SERVER::SetAcceptsConnexions(bool accepts) {
    
    if ( accepts ){
        
        LobbyInstance->StartBroadcast();
        LobbyInstance->StartTCPListen();
    }
    else {
        
        LobbyInstance->StopBroadcast();
        LobbyInstance->StopTCPListen();
    }
}

void NETWORK_SERVER::DispatchMessageToAllPlayers(CORE_DATA_STREAM & data_buffer) {
    
    for(int i = 0; i < THIS_GAME_MAX_NETWORK_PLAYER_SIZE; i ++ ) {
        
        if ( PlayerTable[i] != NULL ) {
            
            size_t size = (int)data_buffer.GetOffset();

            SERVICE_NETWORK_COMMAND * message = new SERVICE_NETWORK_COMMAND();
            message->Data = malloc(size);
            message->Size = (int) size;
            
            memcpy(message->Data, data_buffer.GetMemoryBuffer(),size);
            
            PlayerTable[i]->AppendMessage( message );
        }
    }
}

void NETWORK_SERVER::DispatchMessageToPlayer(NETWORK_PLAYER & player, CORE_DATA_STREAM & data_buffer) {
    
    for(int i = 0; i < THIS_GAME_MAX_NETWORK_PLAYER_SIZE; i ++ ) {
        
        if ( PlayerTable[i] != NULL && player.GetId() == PlayerTable[i]->GetId() ) {
            SERVICE_NETWORK_COMMAND * message = new SERVICE_NETWORK_COMMAND();
            
            PlayerTable[i]->AppendMessage( message );
        }
    }
}

void NETWORK_SERVER::DropPlayer() {
    
}

void NETWORK_SERVER::Update( float time_step ) {

    AccumulatedRemaining += time_step;
    
    LobbyInstance->Update( time_step );
    
    if ( NetworkRefreshRate > 0.0f ) {
    
        if ( NetworkRefreshRate < AccumulatedRemaining ) {
            
            AccumulatedRemaining -=NetworkRefreshRate;
            
            ProcessIncommingMessages();
            ProcessAndSendOutgoingMessages();
        }
    }
    else
    {
        ProcessIncommingMessages();
        ProcessAndSendOutgoingMessages();
    }
}

void NETWORK_SERVER::StopServer() {
    
}

void NETWORK_SERVER::Finalize() {
    
}

/**
 * On new tcp connection
 *      connect the timeline to the network socket
 *      TODO : this is not correct when more than 2 players are in the lobby
 */
void NETWORK_SERVER::OnLobbyTCPNewConnection( SERVICE_NETWORK_CONNECTION * connexion ) {
    
    for (int i = 0; i < THIS_GAME_MAX_NETWORK_PLAYER_SIZE; i++ ) {
        
        if ( PlayerTable[i] == NULL ) {
            
            PlayerTable[i] = new NETWORK_PLAYER(connexion, i, true, false );
            break;
        }
    }
    
    /*ApplicationTimeline.SetOnEventAddedCallback(
                                                CORE_HELPERS_CALLBACK_1<CORE_TIMELINE_EVENT *>( &Wrapper1< MyTestApp, CORE_TIMELINE_EVENT * , &MyTestApp::OnTimelineEventAdded>, this )
                                                );*/
}

void NETWORK_SERVER::OnTCPDataReceived( SERVICE_NETWORK_COMMAND * command ) {
    
    //TODO: check race condition
    IncommingMessageQueue[IncommingMessageQueueIterator++] = command;
    
    if ( IncommingMessageQueueIterator == THIS_GAME_MAX_NETWORK_MESSAG_QUEUE_SIZE ) {
        IncommingMessageQueueIterator = 0;
    }
}

void NETWORK_SERVER::ProcessIncommingMessages() {
    
    SERVICE_NETWORK_COMMAND * command = NULL;
    CORE_TIMELINE_EVENT * event = NULL;
    
    for (int i = 0; i < IncommingMessageQueue.size(); i++ ) {
        
        command = IncommingMessageQueue[i];
        
        if ( command == NULL ) {
            continue;
        }
        
        CORE_DATA_STREAM
            stream( (char *) command->Data, command->Size );
        
        stream.Open();
        stream.OutputBytes(event, command->Size);
        
        stream.Close();
        
        APPLICATION_COMMAND_MANAGER::GetInstance().ProcessEvent( event );
        
        delete IncommingMessageQueue[i];
        IncommingMessageQueue[i] = NULL;
    }
}

void NETWORK_SERVER::ProcessAndSendOutgoingMessages() {
    
    for (int i = 0; i < THIS_GAME_MAX_NETWORK_PLAYER_SIZE; i++ ) {
        
        if ( PlayerTable[i] !=  NULL ) {
            
            LobbyInstance->SendTcpCommand(
                PlayerTable[i]->PrepareMessage(),
                PlayerTable[i]->GetNetworkConnexion() );
        }
    }
}
