//
//  GAME_SERVER.hpp
//  GAME-ENGINE-REBORN
//
//  Created by Christophe Bernard on 29/10/16.
//  Copyright © 2016 Christophe Bernard. All rights reserved.
//

#ifndef NETWORK_SERVER_hpp
#define NETWORK_SERVER_hpp

#include "CORE_HELPERS_CLASS.h"
#include "CORE_DATA_BUFFER.h"
#include "SERVICE_NETWORK_LOBBY.h"
#include "NETWORK_PLAYER.h"
#include "CORE_TIMELINE.h"
#include "APPLICATION_COMMAND_MANAGER.h"

#define THIS_GAME_MAX_NETWORK_PLAYER_SIZE 8
#define THIS_GAME_MAX_NETWORK_MESSAG_QUEUE_SIZE 512

XS_CLASS_BEGIN(NETWORK_SERVER)

NETWORK_SERVER();

void Initialize( float network_refresh_rate );
void SetAcceptsConnexions(bool accepts);

void Update( float time_step );

void Finalize();
void StopServer();

void DispatchMessageToAllPlayers(CORE_DATA_STREAM & data_buffer);
void DispatchMessageToPlayer(NETWORK_PLAYER & player, CORE_DATA_STREAM & data_buffer);

void DropPlayer();


SERVICE_NETWORK_LOBBY * GetLobby() { return LobbyInstance; }

private :

void ProcessIncommingMessages();
void ProcessAndSendOutgoingMessages();

void OnLobbyTCPNewConnection( SERVICE_NETWORK_CONNECTION * stream );
void OnTCPDataReceived( SERVICE_NETWORK_COMMAND * command );

SERVICE_NETWORK_LOBBY
    * LobbyInstance;
std::array<NETWORK_PLAYER *, THIS_GAME_MAX_NETWORK_PLAYER_SIZE>
    PlayerTable;
std::array< SERVICE_NETWORK_COMMAND *, THIS_GAME_MAX_NETWORK_MESSAG_QUEUE_SIZE>
    IncommingMessageQueue;
int
    IncommingMessageQueueIterator;
float
    NetworkRefreshRate,
    AccumulatedRemaining;
CORE_TIMELINE
    NetworkEventsTimeLine;

XS_CLASS_END

#endif /* GAME_SERVER_hpp */
