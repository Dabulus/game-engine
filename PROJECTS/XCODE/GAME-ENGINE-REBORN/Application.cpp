//
//  Application.cpp
//  GAME-ENGINE-REBORN
//
//  Created by Christophe Bernard on 1/12/13.
//  Copyright (c) 2013 Christophe Bernard. All rights reserved.
//

#include "Application.h"

#include "CORE_RUNTIME_ENVIRONMENT.h"
#include "CORE_HELPERS_CLASS.h"
#include "CORE_HELPERS_CALLBACK.h"
#include "CORE_ABSTRACT_PROGRAM_RUNTIME_MANAGER.h"
#include "CORE_MEMORY.h"
#include "CORE_MATH_SHAPE_RECTANGLE.h"
#include "CORE_DATA_STREAM.h"
#include "RESOURCE_IMAGE_PNG_LOADER.h"
#include "AUDIO_SYSTEM.h"
#include "GRAPHIC_UI_SYSTEM.h"
#include "GRAPHIC_SHADER_PROGRAM.h"
#include "PERIPHERIC_INTERACTION_SYSTEM.h"
#include "GAMEPLAY_COMPONENT_SYSTEM.h"
#include "GAMEPLAY_COMPONENT_ENTITY.h"
#include "GAMEPLAY_COMPONENT_POSITION.h"
#include "GAMEPLAY_COMPONENT_SYSTEM.h"
#include "GAMEPLAY_COMPONENT_SYSTEM_UPDATE_POSITION.h"
#include "GAMEPLAY_COMPONENT_SYSTEM_RENDERER.h"
#include "GAMEPLAY_COMPONENT_SYSTEM_ANIMATION_BLENDING.h"
#include "GAMEPLAY_COMPONENT_SYSTEM_ANIMATING.h"
#include "GAMEPLAY_COMPONENT_SYSTEM_PICKING.h"
#include "GAMEPLAY_COMPONENT_ANIMATION.h"
#include "GAMEPLAY_COMPONENT_PHYSICS.h"
#include "GAMEPLAY_COMPONENT_RENDER.h"
#include "GAMEPLAY_COMPONENT_MANAGER.h"
#include "GAMEPLAY_COMPONENT_SYSTEM_COLLISION_DETECTION.h"
#include "GRAPHIC_FONT_MANAGER.h"
#include "CORE_DATA_LOADER.h"
#include "CORE_MEMORY.h"

#include <time.h>

#if PLATFORM_IOS //ie iOS
    #import <Foundation/Foundation.h>
#elif PLATFORM_ANDROID
    #include <android/asset_manager.h>
#endif

#define PARTICLE_SIZE 50000

CORE_ABSTRACT_PROGRAM_BINDER_DECLARE_CLASS( MyTestApp )
    CORE_ABSTRACT_PROGRAM_BINDER_DEFINE_YIELD_METHOD( GAMEPLAY_SCENE::PTR, MyTestApp, GetScene )
CORE_ABSTRACT_PROGRAM_BINDER_END_CLASS( MyTestApp )

MyTestApp::MyTestApp() :
    CORE_APPLICATION(),
    NakedGirlObject(NULL),
    ResourceObject( NULL ),
    PlanObject(NULL),
    CubeObject(NULL),
    HeightMapObject(NULL),
    SphereObject(NULL),
    Line(),
    Scene(NULL),
    Position(),
    Lookat(),
    Camera(),
    LightCamera( NULL ),
    AstroBoy( NULL ),
    MainWindow(),
    InterfaceCamera(),
    RenderTargetCamera(),
    DefaultFileystem(),
    Displacement( CORE_MATH_VECTOR::Zero ),
    ComponentIndex( 0 ),
    BlurEffect(),
    BasicGeometryEffect(),
    DirectionalLight( NULL ),
    PointLightOne( NULL ),
    PointLightTwo( NULL ),
    SpotLightOne( NULL ),
    SpotLightTwo( NULL ),
    Server(),
    Client() {
    
    SERVICE_LOGGER_Error( "ALL APP create 1" );
        
    #if PLATFORM_OSX
        DefaultFileystem.Initialize( "/Users/CBE/DevelopProjects/game-engine/RESOURCES/" );
    #elif PLATFORM_IOS
        DefaultFileystem.Initialize( "None" );
    #elif PLATFORM_ANDROID
        //DefaultFileystem.Initialize( "None" );
    #elif PLATFORM_WINDOWS
        DefaultFileystem.Initialize( "C:\\Users\\X\\Documents\\game-engine\\RESOURCES\\" );
    #endif
        
    CORE_FILESYSTEM::SetDefaultFilesystem( DefaultFileystem );
        
    SERVICE_LOGGER_Error( "ALL APP create 2" );

    SetApplicationInstance( *this );
        
    BoneLevel = 0;
        
    CORE_FIXED_STATE_InitializeState( StateMachine, MyTestApp::INITIAL_STATE, this );
}

MyTestApp::~MyTestApp() {
    
}


CORE_FIXED_STATE_DefineStateEnterEvent( MyTestApp::INITIAL_STATE )

CORE_FIXED_STATE_EndOfStateEvent()


CORE_FIXED_STATE_DefineStateEvent( MyTestApp::INITIAL_STATE, UPDATE_EVENT )
    CORE_FIXED_STATE_MACHINE_ChangeState( ((MyTestApp &) MyTestApp::GetApplicationInstance()).GetStateMachine(), ((MyTestApp &) MyTestApp::GetApplicationInstance()).LOADING_STATE);
CORE_FIXED_STATE_EndOfStateEvent()


CORE_FIXED_STATE_DefineStateLeaveEvent( MyTestApp::INITIAL_STATE )

CORE_FIXED_STATE_EndOfStateEvent()




CORE_FIXED_STATE_DefineStateEnterEvent( MyTestApp::LOADING_STATE )

CORE_FIXED_STATE_EndOfStateEvent()


CORE_FIXED_STATE_DefineStateEvent( MyTestApp::LOADING_STATE, UPDATE_EVENT )
    CORE_FIXED_STATE_MACHINE_ChangeState( ((MyTestApp &) MyTestApp::GetApplicationInstance()).GetStateMachine(), ((MyTestApp &) MyTestApp::GetApplicationInstance()).MENU_STATE);
CORE_FIXED_STATE_EndOfStateEvent()


CORE_FIXED_STATE_DefineStateLeaveEvent( MyTestApp::LOADING_STATE )

CORE_FIXED_STATE_EndOfStateEvent()




CORE_FIXED_STATE_DefineStateEnterEvent( MyTestApp::MENU_STATE )

CORE_FIXED_STATE_EndOfStateEvent()


CORE_FIXED_STATE_DefineStateEvent( MyTestApp::MENU_STATE, UPDATE_EVENT )
    CORE_FIXED_STATE_MACHINE_ChangeState( ((MyTestApp &) MyTestApp::GetApplicationInstance()).GetStateMachine(), ((MyTestApp &) MyTestApp::GetApplicationInstance()).GAME_STATE);
CORE_FIXED_STATE_EndOfStateEvent()

CORE_FIXED_STATE_DefineStateEvent( MyTestApp::MENU_STATE, GOTO_LOBBY )

CORE_FIXED_STATE_EndOfStateEvent()


CORE_FIXED_STATE_DefineStateLeaveEvent( MyTestApp::MENU_STATE )

CORE_FIXED_STATE_EndOfStateEvent()




CORE_FIXED_STATE_DefineStateEnterEvent( MyTestApp::GAME_STATE )

CORE_FIXED_STATE_EndOfStateEvent()


CORE_FIXED_STATE_DefineStateEvent( MyTestApp::GAME_STATE, UPDATE_EVENT )
    CORE_FIXED_STATE_MACHINE_ChangeState( ((MyTestApp &) MyTestApp::GetApplicationInstance()).GetStateMachine(), ((MyTestApp &) MyTestApp::GetApplicationInstance()).PAUSE_STATE );
CORE_FIXED_STATE_EndOfStateEvent()


CORE_FIXED_STATE_DefineStateLeaveEvent( MyTestApp::GAME_STATE )

CORE_FIXED_STATE_EndOfStateEvent()




CORE_FIXED_STATE_DefineStateEnterEvent( MyTestApp::PAUSE_STATE )

CORE_FIXED_STATE_EndOfStateEvent()

CORE_FIXED_STATE_DefineStateEvent( MyTestApp::PAUSE_STATE, UPDATE_EVENT )

CORE_FIXED_STATE_EndOfStateEvent()

CORE_FIXED_STATE_DefineStateLeaveEvent( MyTestApp::PAUSE_STATE )

CORE_FIXED_STATE_EndOfStateEvent()



void MyTestApp::Initialize() {
    
    CORE_MATH_MATRIX m;
    m.YRotate(M_PI);
    Lookat.ToMatrix(m.GetRow(0));
    
    Lookat.Normalize();
    
    SERVICE_LOGGER_Error( "ALL APP Inititialize 0.11" );
    
    CORE_ABSTRACT_PROGRAM_RUNTIME_MANAGER::GetInstance().Initialize();
    CORE_ABSTRACT_RUNTIME_LUA * runtime = (CORE_ABSTRACT_RUNTIME_LUA *) CORE_ABSTRACT_PROGRAM_RUNTIME_MANAGER::GetInstance().getDefaultProgramRuntimeTable()[ CORE_ABSTRACT_PROGRAM_RUNTIME_Lua ];
    
    CORE_ABSTRACT_PROGRAM_BINDER::GetInstance().BindRuntime<CORE_ABSTRACT_RUNTIME_LUA>( *runtime );
    
    SERVICE_LOGGER_Error( "ALL APP Inititialize 1" );
    
    Position.Set( 0.0f, 0.0f, 0.0f, 1.0f);
    
    AUDIO_SYSTEM::GetInstance().Initialize();
    AUDIO_SYSTEM::GetInstance().GetBank().RegisterSoundFilePath(
        CORE_FILESYSTEM_PATH::FindFilePath( "bell-ringing-05" , "mp3", "AUDIO" ),
        ApplicationSoundBank::BellSound,
        AUDIO_BANK_SOUND_LOAD_OPTION_SoundStartupLoad, "mp3" );
    AUDIO_SYSTEM::GetInstance().GetBank().RegisterSoundFilePath(
        CORE_FILESYSTEM_PATH::FindFilePath( "Rammstein-Du-Hast" , "mp3", "AUDIO" ),
        ApplicationSoundBank::RammsteinSound,
        AUDIO_BANK_SOUND_LOAD_OPTION_StartupStreamMusic, "mp3" );
    AUDIO_SYSTEM::GetInstance().GetBank().RegisterSoundFilePath(
        CORE_FILESYSTEM_PATH::FindFilePath( "sound_bubbles" , "wav", "AUDIO" ),
        ApplicationSoundBank::BubbleSound,
        AUDIO_BANK_SOUND_LOAD_OPTION_SoundStartupLoad, "wav" );
    AUDIO_SYSTEM::GetInstance().GetBank().RegisterSoundFilePath(
        CORE_FILESYSTEM_PATH::FindFilePath( "sound_electric" , "wav", "AUDIO" ),
        ApplicationSoundBank::ElectricSound,
        AUDIO_BANK_SOUND_LOAD_OPTION_SoundStartupLoad, "wav" );
    
    AUDIO_SYSTEM::GetInstance().GetBank().Load();
    
    Scene = new GAMEPLAY_SCENE();
    
    GAMEPLAY_COMPONENT_SYSTEM_COLLISION_DETECTION * bullet_system = new GAMEPLAY_COMPONENT_SYSTEM_COLLISION_DETECTION;
    
    bullet_system->Initialize();
    
    Scene->InsertUpdatableSystem( new GAMEPLAY_COMPONENT_SYSTEM_UPDATE_POSITION );
    Scene->InsertUpdatableSystem( new GAMEPLAY_COMPONENT_SYSTEM_ANIMATING );
    Scene->InsertUpdatableSystem( new GAMEPLAY_COMPONENT_SYSTEM_PICKING );
    Scene->InsertUpdatableSystem( bullet_system );
    
    Scene->InsertRenderableSystem( new GAMEPLAY_COMPONENT_SYSTEM_RENDERER );

    GAMEPLAY_COMPONENT_MANAGER::GetInstance().Initialize();
    
    CORE_HELPERS_CALLBACK * myCallback = new CORE_HELPERS_CALLBACK( &Wrapper<CORE_APPLICATION, &CORE_APPLICATION::Render>, this );
    SERVICE_LOGGER_Error( "Camera : %d %d Lookat : %.2f %.2f %.2f %.2f ", ApplicationWindow->GetWidth(), ApplicationWindow->GetHeight(), Lookat.X(), Lookat.Y(), Lookat.Z(), Lookat.W() );
    Camera = new GRAPHIC_CAMERA( 1.0f, 10000.0f, ApplicationWindow->GetWidth(), ApplicationWindow->GetHeight(), Position, Lookat );
    LightCamera = new GRAPHIC_CAMERA_ORTHOGONAL( -256.0f, 256.0f, 256.0f, 256.0f, Position, Lookat );
    
    CORE_MATH_QUATERNION interface_lookat( 0.0f, 0.0f, 0.0f, 1.0f );
    
    interface_lookat.Normalize();
    
    RenderTargetCamera = new GRAPHIC_CAMERA_ORTHOGONAL( 1.0f, 100.0f, ApplicationWindow->GetWidth(), ApplicationWindow->GetHeight(), Position, interface_lookat );
    InterfaceCamera = new GRAPHIC_CAMERA_ORTHOGONAL( 1.0f, 100.0f, ApplicationWindow->GetWidth(), ApplicationWindow->GetHeight(), Position, interface_lookat );
    
    DirectionalLight = new GRAPHIC_SHADER_LIGHT;
    
    CORE_MATH_VECTOR diffuse(1.0f, 1.0f, 1.0f, 1.0f);
    CORE_MATH_VECTOR direction(0.0f, -1.0f, 0.0f, 1.0f);
    
    DirectionalLight->InitializeDirectional( diffuse, direction, 0.5f, 0.5f);
    
    CORE_MATH_VECTOR diffuse_1(0.9f, 0.0f, 0.0f, 1.0f);
    CORE_MATH_VECTOR diffuse_2(0.0f, 0.0f, 0.9f, 1.0f);
    
    CORE_MATH_VECTOR direction_1(0.0f, 1.0f, 0.0f, 0.0f);
    CORE_MATH_VECTOR direction_2(0.0f, -1.0f, 0.0f, 0.0f);
    
    CORE_MATH_VECTOR point1_position(-10.0f, 0.0f, 0.0f, 1.0f);
    CORE_MATH_VECTOR point2_position(10.0f, 0.0f, 0.0f, 1.0f);
    
    PointLightOne = new GRAPHIC_SHADER_LIGHT;
    PointLightOne->InitializePoint(diffuse_1, point1_position, 0.001f, 0.01f, 0.5f, 1.0f, 1.0f);
    
    PointLightTwo = new GRAPHIC_SHADER_LIGHT;
    PointLightTwo->InitializePoint(diffuse_2, point2_position, 0.001f, 0.01f, 0.5f, 1.0f, 1.0f);
    
    SpotLightOne = new GRAPHIC_SHADER_LIGHT;
    SpotLightOne->InitializeSpot(diffuse_1, point1_position, direction_1, 0.1f, 0.2f, 0.4f, 0.001f, 1.0f, 1.0f );
    
    SpotLightTwo = new GRAPHIC_SHADER_LIGHT;
    SpotLightTwo->InitializeSpot(diffuse_2, point2_position, direction_2, 0.1f, 0.2f, 0.9f, 0.1f, 1.0f, 1.0f );

    GRAPHIC_RENDERER::GetInstance().SetCamera( Camera );
    GRAPHIC_RENDERER::GetInstance().Initialize();
    GRAPHIC_RENDERER::GetInstance().SetRenderCallback( myCallback );
    GRAPHIC_RENDERER::GetInstance().SetDirectionalLight( DirectionalLight );
    GRAPHIC_RENDERER::GetInstance().SetPointLight( PointLightOne, 0 );
    GRAPHIC_RENDERER::GetInstance().SetPointLight( PointLightTwo, 1 );
    GRAPHIC_RENDERER::GetInstance().SetSpotLight( SpotLightOne, 0 );
    GRAPHIC_RENDERER::GetInstance().SetSpotLight( SpotLightTwo, 1 );

    InitializeGraphics();
    
    CORE_HELPERS_CALLBACK_1< GAMEPLAY_COMPONENT_ENTITY * > * picking_allback =
        new CORE_HELPERS_CALLBACK_1< GAMEPLAY_COMPONENT_ENTITY * >(
            &Wrapper1< MyTestApp, GAMEPLAY_COMPONENT_ENTITY * , &MyTestApp::OnObjectPicked>, this );
    
    for ( int i = 0; i < 3; i++ ) {
    
        GAMEPLAY_COMPONENT_ENTITY * component_entity = GAMEPLAY_COMPONENT_MANAGER::GetInstance().CreateEntity();
        
        component_entity->SetIndex( ComponentIndex++ );
        
        //should not be done like this
        
        component_entity->SetCompononent( GAMEPLAY_COMPONENT::FactoryCreate( GAMEPLAY_COMPONENT_TYPE_Position ), GAMEPLAY_COMPONENT_TYPE_Position );
        component_entity->SetCompononent( GAMEPLAY_COMPONENT::FactoryCreate( GAMEPLAY_COMPONENT_TYPE_Render ), GAMEPLAY_COMPONENT_TYPE_Render );
        component_entity->SetCompononent( GAMEPLAY_COMPONENT::FactoryCreate( GAMEPLAY_COMPONENT_TYPE_Physics ), GAMEPLAY_COMPONENT_TYPE_Physics );

        ( ( GAMEPLAY_COMPONENT_RENDER *) component_entity->GetComponent(GAMEPLAY_COMPONENT_TYPE_Render))->SetObject( CubeObject );
        
        GAMEPLAY_COMPONENT_SYSTEM_RENDERER * render_system = ( GAMEPLAY_COMPONENT_SYSTEM_RENDERER * ) Scene->GetRenderableSystemTable()[0];
        
        render_system->AddEntity( component_entity );
        render_system->SetRenderer( &GRAPHIC_RENDERER::GetInstance() );
        
        CORE_MATH_VECTOR position( 0.0f + i * 10.0f, 0.0f, -30.0f, 1.0f );
        
        ( ( GAMEPLAY_COMPONENT_SYSTEM_PICKING * ) Scene->GetUpdatableSystemTable()[2])->AddEntity( component_entity );
        ( ( GAMEPLAY_COMPONENT_SYSTEM_PICKING * ) Scene->GetUpdatableSystemTable()[2])->SetOnPickedCallback( picking_allback );
        
        ( ( GAMEPLAY_COMPONENT_PHYSICS *) component_entity->GetComponent(GAMEPLAY_COMPONENT_TYPE_Physics))->ConfigureShapeSphere( position );
        
        bullet_system->AddEntity( component_entity );
        
        component_entity->SetPosition( position );
    }
    
    CreateGround();
    CreateNakedGirl();
    CreateMoulin();
    
    
    {
        SERVICE_LOGGER_Error( "ALL APP Inititialize 1.9" );
        
        GRAPHIC_FONT_MANAGER::GetInstance().LoadFont(
                                                     CORE_HELPERS_UNIQUE_IDENTIFIER( "arial_black_12" ),
        CORE_FILESYSTEM_PATH::FindFilePath( "arial_black_12" , "fxb", "FONTS/" ),
        CORE_FILESYSTEM_PATH::FindFilePath( "arial_black_12" , "png", "FONTS/" ) );
        
        MainWindow.GetPlacement().Initialize( NULL,
                                             CORE_MATH_VECTOR::Zero,
                                             CORE_MATH_VECTOR( ApplicationWindow->GetWidth(), ApplicationWindow->GetHeight() ),
                                             GRAPHIC_UI_Center );
        
        SERVICE_NETWORK_SYSTEM::GetInstance().Initialize();
        
        SERVICE_LOGGER_Error( "ALL APP Inititialize 2" );
        
        PreviousPosition = Camera->GetPosition();
        
        CORE_FILESYSTEM_PATH path = CORE_FILESYSTEM_PATH::FindFilePath( "game" , "lua", "SCRIPTS" );
        
        SERVICE_LOGGER_Error( "ALL APP Inititialize 2.22" );
        
        TestEntity.Initialize( path );
        
        MainWindow.SetScreenSize(CORE_MATH_VECTOR( GetApplicationWindow().GetWidth(), GetApplicationWindow().GetHeight() ) );
        GRAPHIC_UI_SYSTEM::GetInstance().RegisterScreen( &MainWindow, "MainWindow" );
        
        SERVICE_LOGGER_Error( "ALL APP Inititialize 2.23" );
        
        MainWindow.Initialize();
        
        //GRAPHIC_UI_SYSTEM::GetInstance().RegisterScritpedScreen( CORE_FILESYSTEM_PATH::FindFilePath( "APPLICATION_OPTION_WINDOW" , "lua", "SCRIPTS" ), "APPLICATION_OPTION_WINDOW" );
        
        Server.Initialize(1.0f / 25.0f );
        
        MainWindow.SetNetworkServer( &Server );
        MainWindow.SetNetworkClient( &Client );
        /*LuaScript = CORE_ABSTRACT_PROGRAM_MANAGER::GetInstance().LoadProgram(
                                                                             path.GetPath(), CORE_ABSTRACT_PROGRAM_RUNTIME_Lua
                                                                             );*/
        
        //LuaScript->Execute();
        
        //runtime->ExecuteCommand( "AUDIO_SYSTEM:GetInstance():PlayMusic( \"RammsteinSound\" )" );
    }
}

void MyTestApp::OnObjectPicked( GAMEPLAY_COMPONENT_ENTITY * entity  ) {
    
    CORE_MATH_VECTOR new_position( rand() % 10, rand() % 10, -30.0f, 1.0f );
    
    APPLICATION_COMMAND_MANAGER::GetInstance().CommandMoveCube( new_position, entity );
}

void MyTestApp::Finalize() {

    Server.Finalize();
    Client.Finalize();
    
    GRAPHIC_RENDERER::RemoveInstance();
    CORE_ABSTRACT_PROGRAM_MANAGER::RemoveInstance();
    CORE_ABSTRACT_PROGRAM_RUNTIME_MANAGER::RemoveInstance();
    
    SERVICE_NETWORK_SYSTEM::GetInstance().Finalize();
    SERVICE_NETWORK_SYSTEM::RemoveInstance();
    
    GAMEPLAY_COMPONENT_MANAGER::RemoveInstance();
    
    CORE_ABSTRACT_PROGRAM_BINDER::RemoveInstance();
    
    AUDIO_SYSTEM::GetInstance().Finalize();
    AUDIO_SYSTEM::RemoveInstance();
    
    DefaultFileystem.Finalize();
    
    NakedGirlObject->Release();
    //AstroBoy->Release();
    
    CubeObject->Release();
    HeightMapObject->Release();
    SphereObject->Release();
    Line->Release();
    PlanObject->Release();
    EffectPlan->Release();
    
    if ( ResourceObject ) {
        
        ResourceObject->Release();
    }
    
    GRAPHIC_SYSTEM::ReleaseTexture( NakedGirlObject->GetMeshTable()[0]->GetTexture() );
    GRAPHIC_SYSTEM::ReleaseTexture( NakedGirlObject->GetMeshTable()[1]->GetTexture() );
    GRAPHIC_SYSTEM::ReleaseTexture( NakedGirlObject->GetMeshTable()[2]->GetTexture() );
    GRAPHIC_SYSTEM::ReleaseTexture( NakedGirlObject->GetMeshTable()[3]->GetTexture() );
    GRAPHIC_SYSTEM::ReleaseTexture( NakedGirlObject->GetMeshTable()[4]->GetTexture() );
    GRAPHIC_SYSTEM::ReleaseTexture( NakedGirlObject->GetMeshTable()[5]->GetTexture() );
    
    GRAPHIC_SYSTEM::ReleaseTexture( NakedGirlObject->GetMeshTable()[0]->GetNormalTexture() );
    GRAPHIC_SYSTEM::ReleaseTexture( NakedGirlObject->GetMeshTable()[1]->GetNormalTexture() );
    GRAPHIC_SYSTEM::ReleaseTexture( NakedGirlObject->GetMeshTable()[2]->GetNormalTexture() );
    GRAPHIC_SYSTEM::ReleaseTexture( NakedGirlObject->GetMeshTable()[3]->GetNormalTexture() );
    GRAPHIC_SYSTEM::ReleaseTexture( NakedGirlObject->GetMeshTable()[4]->GetNormalTexture() );
    GRAPHIC_SYSTEM::ReleaseTexture( NakedGirlObject->GetMeshTable()[5]->GetNormalTexture() );

    CORE_MEMORY_ObjectSafeDeallocation( ResourceObject );
    CORE_MEMORY_ObjectSafeDeallocation( NakedGirlObject );
    CORE_MEMORY_ObjectSafeDeallocation( PlanObject);
    CORE_MEMORY_ObjectSafeDeallocation( EffectPlan );
    CORE_MEMORY_ObjectSafeDeallocation( CubeObject );
    CORE_MEMORY_ObjectSafeDeallocation( HeightMapObject );
    CORE_MEMORY_ObjectSafeDeallocation( SphereObject );
    
    CORE_MEMORY_ObjectSafeDeallocation( Line );
    
    CORE_MEMORY_ObjectSafeDeallocation( DirectionalLight );
    CORE_MEMORY_ObjectSafeDeallocation( PointLightOne );
    CORE_MEMORY_ObjectSafeDeallocation( PointLightTwo);
    CORE_MEMORY_ObjectSafeDeallocation( SpotLightOne );
    CORE_MEMORY_ObjectSafeDeallocation( SpotLightTwo );
    
    CORE_MEMORY_ObjectSafeDeallocation( TextureBlock );
    CORE_MEMORY_ObjectSafeDeallocation( AlternateTextureBlock );
    
    CORE_MEMORY_ObjectSafeDeallocation( Scene );
    CORE_MEMORY_ObjectSafeDeallocation( Camera );
    CORE_MEMORY_ObjectSafeDeallocation( InterfaceCamera );
    CORE_MEMORY_ObjectSafeDeallocation( RenderTargetCamera );
}

void MyTestApp::Render() {
    
    CORE_MATH_MATRIX current_mat( &Camera->GetProjectionMatrix()[0] );

    CORE_MATH_MATRIX inv( CORE_MATH_MATRIX::Identity );
    
    current_mat *= Camera->GetViewMatrix();
    current_mat.GetInverse( inv );
    
    #if PLATFORM_OSX
        memcpy(
               (void*) BlurEffect->GetProgram().getShaderAttribute( GRAPHIC_SHADER_EFFECT_SPEEDBLUR::InverseCurrentModelViewIdentifier ).AttributeValue.Value.FloatMatrix4x4,
               (void*) &inv[0],
               16* sizeof(float) );
    #endif
    
    {
        #if PLATFORM_OSX
            {
                GRAPHIC_RENDERER::GetInstance().SetCamera( LightCamera );
                GRAPHIC_RENDERER::GetInstance().SetPassIndex(1);
                
                ShadowMapRenderTarget.Apply();
                    Scene->Render();
                static int c = 0;
                c++;
                if ( c % 100 == 0 ) {
                    
                    //ShadowMapRenderTarget.TargetTexture->SaveTo( CORE_FILESYSTEM_PATH::FindFilePath("rt", "png", "") );
                }
                ShadowMapRenderTarget.Discard();
            }
        #endif
        
        {
            
            GRAPHIC_RENDERER::GetInstance().SetPassIndex(0);
            GRAPHIC_RENDERER::GetInstance().SetCamera( Camera );
            
            #if PLATFORM_OSX
                GRAPHIC_RENDERER::GetInstance().SetShadowMapCamera(LightCamera);
                GRAPHIC_RENDERER::GetInstance().SetDepthTexture(ShadowMapRenderTarget.TargetTexture);
            #endif
            //EffectPlan->GetShaderTable()[0] = &BasicGeometryEffect->GetProgram();
            //EffectPlz
            
            Scene->Render();
            
            #if PLATFORM_OSX
                GRAPHIC_RENDERER::GetInstance().SetShadowMapCamera( NULL );
                GRAPHIC_RENDERER::GetInstance().SetDepthTexture( NULL );
            #endif
            
        }
        //ShadowMapRenderTarget.Apply();
        
        
        /*GAMEPLAY_COMPONENT_POSITION * component;
        GAMEPLAY_COMPONENT_RENDER * component_grap;
        
        GRAPHIC_SYSTEM::SetPolygonMode( GRAPHIC_SYSTEM_POLYGON_FILL_MODE_Line );
        component = ( GAMEPLAY_COMPONENT_POSITION * ) component_entity->GetComponent( GAMEPLAY_COMPONENT_TYPE_Position );
        component_grap = ( GAMEPLAY_COMPONENT_RENDER * ) component_entity->GetComponent( GAMEPLAY_COMPONENT_TYPE_Render );
        
        for (int i = 0; i < NakedGirlObject->GetMeshTable().size(); i++ ) {
            
            GAMEPLAY_COMPONENT_POSITION * component = ( GAMEPLAY_COMPONENT_POSITION * ) component_entity->GetComponent( GAMEPLAY_COMPONENT_TYPE_Position );
            
            CubeObject->SetPosition( NakedGirlObject->GetMeshTable()[i]->GetBoundingShape().GetPosition() + component->GetPosition() );
            CubeObject->SetOrientation( NakedGirlObject->GetMeshTable()[i]->GetBoundingShape().GetOrientation() + component->GetOrientation() );
            CubeObject->SetScaleFactor(NakedGirlObject->GetMeshTable()[i]->GetBoundingShape().GetHalfDiagonal() );
            
            CubeObject->Render( GRAPHIC_RENDERER::GetInstance() );
        }
        

        component = ( GAMEPLAY_COMPONENT_POSITION * ) component_entity2->GetComponent( GAMEPLAY_COMPONENT_TYPE_Position );
        component_grap = ( GAMEPLAY_COMPONENT_RENDER * ) component_entity2->GetComponent( GAMEPLAY_COMPONENT_TYPE_Render );
        
        for (int i = 0; i < component_grap->GetObject()->GetMeshTable().size(); i++ ) {
            
            CubeObject->SetPosition( component_grap->GetObject()->GetMeshTable()[i]->GetBoundingShape().GetPosition() + component->GetPosition() );
            CubeObject->SetOrientation( component->GetOrientation() );
            
            if ( i == 4 ) {
                
                CORE_MATH_MATRIX & tr = component_grap->GetObject()->GetMeshTable()[i]->GetTransform();
                
                tr.ZRotate( 0.02f );
            }
            
            CORE_MATH_MATRIX mat;
            
            component_grap->GetObject()->GetMeshTable()[i]->GetBoundingShape().GetOrientation().ToMatrix( mat.GetRow(0) );
            
            CubeObject->GetMeshTable()[0]->SetTransform( component_grap->GetObject()->GetMeshTable()[i]->GetTransform() * mat );
            
            CubeObject->UpdateGeometry( component_grap->GetObject()->GetMeshTable()[i]->GetBoundingShape().GetPosition(), component_grap->GetObject()->GetMeshTable()[i]->GetBoundingShape().GetHalfDiagonal() );
            
            CubeObject->Render( GRAPHIC_RENDERER::GetInstance() );
        }
        
        GRAPHIC_SYSTEM::SetPolygonMode( GRAPHIC_SYSTEM_POLYGON_FILL_MODE_Full );
        */
        
        //PrimaryRenderTarget.Discard();
    }
    
    #if 0 && PLATFORM_OSX
    GRAPHIC_RENDERER::GetInstance().SetCamera( RenderTargetCamera );

    {
        EffectPlan->GetShaderTable()[0] = &BloomEffect->GetProgram();
        EffectPlan->SetEffect( BloomEffect );
        TextureBlock->SetTexture( PrimaryRenderTarget.TargetTexture );
        
        BloomRenderTarget.Apply();
        EffectPlan->Render( GRAPHIC_RENDERER::GetInstance() );
        BloomRenderTarget.Discard();
    }
    
    {
        EffectPlan->GetShaderTable()[0] = &HorizontalBlurEffect->GetProgram();
        EffectPlan->SetEffect( HorizontalBlurEffect );
        TextureBlock->SetTexture( BloomRenderTarget.TargetTexture );
        
        GaussianRenderTarget.Apply();
        EffectPlan->Render( GRAPHIC_RENDERER::GetInstance() );
        GaussianRenderTarget.Discard();
    }
    
    {
        EffectPlan->GetShaderTable()[0] = &VerticalBlurEffect->GetProgram();
        EffectPlan->SetEffect(
                              VerticalBlurEffect );
        TextureBlock->SetTexture( GaussianRenderTarget.TargetTexture );
        
        GaussianRenderTarget.Apply();
        EffectPlan->Render( GRAPHIC_RENDERER::GetInstance() );
        GaussianRenderTarget.Discard();
    }
    
    {
        EffectPlan->GetShaderTable()[0] = &CombineBloomEffect->GetProgram();
        EffectPlan->SetEffect( CombineBloomEffect );
        TextureBlock->SetTexture( PrimaryRenderTarget.TargetTexture );
        
        EffectPlan->SetSecondTextureBlock( AlternateTextureBlock );
        EffectPlan->Render( GRAPHIC_RENDERER::GetInstance() );
        EffectPlan->SetSecondTextureBlock( NULL );
    }
    #endif
    
    GRAPHIC_PARTICLE_SYSTEM::GetInstance().Render(GRAPHIC_RENDERER::GetInstance());
    
    GRAPHIC_RENDERER::GetInstance().SetCamera( InterfaceCamera );
    
    MainWindow.Render( GRAPHIC_RENDERER::GetInstance() );
    
    GRAPHIC_RENDERER::GetInstance().SetCamera( Camera );
    
    Line->SetPosition( ( ( GAMEPLAY_COMPONENT_SYSTEM_PICKING * ) Scene->GetUpdatableSystemTable()[2])->GetRay().GetOrigin() );
    Line->SetTarget( ( ( GAMEPLAY_COMPONENT_SYSTEM_PICKING * ) Scene->GetUpdatableSystemTable()[2])->GetRay().GetDirection() );
    Line->Render(GRAPHIC_RENDERER::GetInstance());
    
    CORE_MATH_MATRIX previous_mat( &Camera->GetProjectionMatrix()[0] );
    
    previous_mat *= Camera->GetViewMatrix();
    
    #if PLATFORM_OSX
        memcpy(
           (void*) BlurEffect->GetProgram().getShaderAttribute( GRAPHIC_SHADER_EFFECT_SPEEDBLUR::PreviousModelViewProjectionIdentifier ).AttributeValue.Value.FloatMatrix4x4,
           (void*) &previous_mat[0],
           16* sizeof(float) );
    #endif
}

void MyTestApp::Update( float time_step ) {
    
    static float acc = 0.0f;
    
    acc += time_step;

    CORE_MATH_QUATERNION q;
    
    q.X(1.0f);
    q.Normalize();
    LightCamera->UpdateCamera(CORE_MATH_VECTOR( 0.0f, 0.0f, 0.0f, 1.0f), q);
    
    Camera->UpdateCamera(Position, Lookat);
    
    GRAPHIC_PARTICLE_SYSTEM::GetInstance().Update(time_step, Position, Lookat);
    UPDATE_EVENT ev(time_step);
    StateMachine.DispatchEvent( ev );
    
    CORE_MATH_VECTOR direction;
    CORE_MATH_MATRIX inverse;
    
    if ( IsClient ) {
        
        Client.Update( time_step );
    }
    else
    {
        Server.Update( time_step );
    }
    
    CORE_MATH_VECTOR deltaPosition = ( Camera->GetPosition() - PreviousPosition );
    
    PreviousPosition = Camera->GetPosition();
    
    GAMEPLAY_COMPONENT_POSITION * pos = (GAMEPLAY_COMPONENT_POSITION *) GAMEPLAY_COMPONENT_MANAGER::GetInstance().GetEntity( 1 )->GetComponent( GAMEPLAY_COMPONENT_TYPE_Position );
    
    pos->SetOrientation(CORE_MATH_QUATERNION( 0.0f, acc, 0.0, 1.0f ) );
    
    #if PLATFORM_OSX
        memcpy(
            (void*) BlurEffect->GetProgram().getShaderAttribute( GRAPHIC_SHADER_EFFECT_SPEEDBLUR::ViewRayIdentifier ).AttributeValue.Value.FloatArray4,
            (void*) &deltaPosition[0],
            4* sizeof(float) );
        
        //printf ( "%f %f %f\n", deltaPosition[0], deltaPosition[1], deltaPosition[2]);
    #endif

    SERVICE_NETWORK_SYSTEM::GetInstance().Update( false );
    
    direction = CORE_MATH_VECTOR::Zero;
    
    direction.Set( 250.0f, 0.0f, 0.0f, 0.0f );
    
    static float accumumated_time = 0.0f;
    
    CORE_MATH_QUATERNION rotation_quat;
    CORE_MATH_MATRIX rotation_mat( CORE_MATH_MATRIX::Identity );
    
    rotation_quat.X( 0.0f );
    rotation_quat.Y( 0.0f );
    rotation_quat.Z( 0.0f );
    rotation_quat.W( 1.0f );
    
    //CORE_MATH_VECTOR light_dir( ( ( GAMEPLAY_COMPONENT_SYSTEM_PICKING * ) Scene->GetUpdatableSystemTable()[2])->GetRay().GetDirection() );
    
    CORE_MATH_VECTOR light_dir( sinf(acc), 0.0f, cosf( acc), 1.0f );
    
    light_dir.Normalize();
    
    DirectionalLight->InternalLight.Directional.Direction[0] = -light_dir[0];
    DirectionalLight->InternalLight.Directional.Direction[1] = -light_dir[0];
    DirectionalLight->InternalLight.Directional.Direction[2] = -light_dir[2];
    
    PointLightOne->InternalLight.Point.Position[0] = -5.0f * cosf( acc * 1.33f);
    PointLightTwo->InternalLight.Point.Position[0] = 5.0f * cosf( acc* 0.5f);
    
    SpotLightOne->InternalLight.Spot.Direction[0] = -light_dir[0];
    SpotLightOne->InternalLight.Spot.Direction[1] = -light_dir[0];
    
    SpotLightTwo->InternalLight.Spot.Direction[0] = -light_dir[0];
    SpotLightTwo->InternalLight.Spot.Direction[1] = -light_dir[0];
    
    static CORE_MATH_VECTOR vector;
    vector = PERIPHERIC_INTERACTION_SYSTEM::GetInstance().GetMouse().GetScreenCoordinates();
    
    rotation_mat.XRotate( (vector[1] - 0.5f ) * M_PI_2 );
    rotation_mat.YRotate( M_PI_2 + (vector[0] - 0.5f )  * M_PI_2 );
    
    rotation_mat.GetInverse(inverse);
    rotation_quat.FromMatrix( &inverse[0] );
    rotation_quat.Normalize();
    
    Lookat[0] = rotation_quat[0];
    Lookat[1] = -1.0f +rotation_quat[1];
    Lookat[2] = rotation_quat[2];
    Lookat[3] = rotation_quat[3];
    
    Lookat.Normalize();
    Lookat.ToMatrix( rotation_mat.GetRow(0) );
    
    CORE_MATH_VECTOR result(direction * rotation_mat);
    
    if ( PERIPHERIC_INTERACTION_SYSTEM::GetInstance().GetKeyboard().IsKeyPressed( KEYBOARD_KEY_CHAR_Z ) ) {
        
        CORE_MATH_VECTOR add;
        
        add[0] =result[0] * time_step;
        add[1] =result[1] * time_step;
        add[2] =result[2] * time_step;
        
        Position += add;
    }
    else if ( PERIPHERIC_INTERACTION_SYSTEM::GetInstance().GetKeyboard().IsKeyPressed(KEYBOARD_KEY_CHAR_S ) ) {
        
        CORE_MATH_VECTOR add;
        
        add[0] =result[0] * time_step;
        add[1] =result[1] * time_step;
        add[2] =result[2] * time_step;
        
        Position -= add;
    }
    
    if ( PERIPHERIC_INTERACTION_SYSTEM::GetInstance().GetKeyboard().IsKeyPressed( KEYBOARD_KEY_CHAR_A ) ) {
        
        Position.Set( Position[0], Position[1] + time_step * 250.0f, Position[2], 1.0f);
    }
    else if ( PERIPHERIC_INTERACTION_SYSTEM::GetInstance().GetKeyboard().IsKeyPressed( KEYBOARD_KEY_CHAR_E ) ) {
        
        Position.Set( Position[0], Position[1] - time_step * 250.0f, Position[2], 1.0f);
    }
    
    if ( PERIPHERIC_INTERACTION_SYSTEM::GetInstance().GetKeyboard().IsKeyPressed(KEYBOARD_KEY_CHAR_Q ) ) {
        
        CORE_MATH_VECTOR add;
        CORE_MATH_MATRIX rot;
        
        add[0] =result[0] * time_step;
        add[1] =result[1] * time_step;
        add[2] =result[2] * time_step;
        
        rot.YRotate(-M_PI_2);
        
        Position += add*rot;
    }
    else if ( PERIPHERIC_INTERACTION_SYSTEM::GetInstance().GetKeyboard().IsKeyPressed(KEYBOARD_KEY_CHAR_D ) ) {
        
        CORE_MATH_VECTOR add;
        CORE_MATH_MATRIX rot;
        
        add[0] =result[0] * time_step;
        add[1] =result[1] * time_step;
        add[2] =result[2] * time_step;
        
        rot.YRotate(-M_PI_2);
        
        Position -= add*rot;
    }
    
    Position += Displacement * 5.0f;

    Camera->UpdateCamera(Position, Lookat);
    
    Scene->Update( time_step );
    
    accumumated_time += time_step;
    
    if ( PERIPHERIC_INTERACTION_SYSTEM::GetInstance().GetMouse().GetLeftButtonClicked() ) {
        
    }
    
    if ( PERIPHERIC_INTERACTION_SYSTEM::GetInstance().GetKeyboard().IsKeyPressed(KEYBOARD_KEY_CHAR_P ) ) {
        BoneLevel++;
    }
    else if ( PERIPHERIC_INTERACTION_SYSTEM::GetInstance().GetKeyboard().IsKeyPressed(KEYBOARD_KEY_CHAR_M ) )
    {
        BoneLevel--;
    }
    
    if ( BoneLevel > 0 ) {
        
        CORE_MATH_QUATERNION quat(accumumated_time, 0.0f, 0.0f, 0.0f);
        
        quat.Normalize();
        
        quat.ToMatrix( (float* ) ((GRAPHIC_OBJECT_ANIMATED *)NakedGirlObject)->GetAnimationController()->GetAnimation( 0 )->GetSkeleton().GetRootSubSkeleton().SubSkelettonTable[0].Joint->GetInterPolatedMatrix() );
    }
    
    MainWindow.Update( time_step );
    
    PERIPHERIC_INTERACTION_SYSTEM::GetInstance().Update();
    AUDIO_SYSTEM::GetInstance().Update( time_step );
}

void MyTestApp::InitializeGraphics() {
    
    GLOBAL_RESOURCES::GetInstance().Initialize();
    
    SERVICE_LOGGER_Error( "ALL APP InitializeGraphics 1" );
    
    RESOURCE_IMAGE_PNG_LOADER loader;

    SERVICE_LOGGER_Error( "Trying to load resource 1" );
    NakedGirlObject = CreateAnimatedObject( CORE_FILESYSTEM_PATH::FindFilePath( "DefenderLingerie00" , "smx", "MODELS" ), CORE_FILESYSTEM_PATH::FindFilePath( "DefenderLingerie00.DE_Lingerie00_Skeleto" , "abx", "MODELS" ));
    //NakedGirlObject = CreateAnimatedObject( CORE_FILESYSTEM_PATH::FindFilePath( "Chris" , "smx", "MODELS" ), CORE_FILESYSTEM_PATH::FindFilePath( "Chris.geom-chris-base-skin1" , "abx", "MODELS" ));
    
    Moulin = GRAPHIC_MESH_MANAGER::GetInstance().LoadObject( CORE_FILESYSTEM_PATH::FindFilePath( "MoulinNoAnim" , "smx", "MODELS" ), 1337, GRAPHIC_MESH_TYPE_ModelResource);
    
    SERVICE_LOGGER_Error( "loaded resource 1" );
    
    GRAPHIC_SHADER_EFFECT::PTR effect = GRAPHIC_SHADER_EFFECT::LoadResourceForPath(CORE_HELPERS_UNIQUE_IDENTIFIER( "SHADER::Default"), CORE_FILESYSTEM_PATH::FindFilePath( "Shader" , "vsh", "OPENGL2" ) );
    
    GRAPHIC_SHADER_EFFECT::PTR effect_shadow_map = GRAPHIC_SHADER_EFFECT::LoadEffectWithVertexAndFragmentPath( CORE_FILESYSTEM_PATH::FindFilePath( "ShaderShadowMap" , "vsh", "OPENGL2" ),CORE_FILESYSTEM_PATH::FindFilePath( "ShadowMapEffect" , "fsh", "OPENGL2" ),
            CORE_HELPERS_UNIQUE_IDENTIFIER( "SHADER::ShadowMapShader"));
    
    GRAPHIC_SHADER_EFFECT::PTR BasicEffectShadowMap = GRAPHIC_SHADER_EFFECT::LoadEffectWithVertexAndFragmentPath(
        CORE_FILESYSTEM_PATH::FindFilePath( "BasicGeometrySMShader" , "vsh", "OPENGL2" ),
        CORE_FILESYSTEM_PATH::FindFilePath( "ShadowMapEffect" , "fsh", "OPENGL2" ),
        CORE_HELPERS_UNIQUE_IDENTIFIER( "SHADER::BaicShadowMapShader"));
    
    GRAPHIC_SHADER_EFFECT::PTR basic_geometry_effect = GRAPHIC_SHADER_EFFECT::LoadResourceForPath(CORE_HELPERS_UNIQUE_IDENTIFIER( "SHADER::BasicGeometryShader"), CORE_FILESYSTEM_PATH::FindFilePath( "BasicGeometryShader" , "vsh", "OPENGL2" ) );
    
    basic_geometry_effect->Initialize( GRAPHIC_SHADER_BIND_PositionNormal );
    BasicEffectShadowMap->Initialize( GRAPHIC_SHADER_BIND_PositionNormal );
    
    for ( int par = 0; par < NakedGirlObject->GetMeshTable().size(); par++  ) {
        
        NakedGirlObject->GetMeshTable()[par]->CreateBuffers();
    }
    
    /*for ( int par = 0; par < AstroBoy->GetMeshTable().size(); par++  ) {
        
        AstroBoy->GetMeshTable()[par]->CreateBuffers();
    }*/
    
    for ( int par = 0; par < Moulin->GetMeshTable().size(); par++  ) {
        
        Moulin->GetMeshTable()[par]->CreateBuffers();
    }
    
    RESOURCE_IMAGE * image = NULL;

    image = RESOURCE_IMAGE::LoadResourceForPath( "HANDS_DE_GL_N_00", CORE_FILESYSTEM_PATH::FindFilePath( "HANDS_DE_GL_N_00", "png", "MODEL_TEXTURES" ) );
    NakedGirlObject->GetMeshTable()[0]->SetTexture( image->CreateTextureObject( true ) );
    
    image=RESOURCE_IMAGE::LoadResourceForPath( "HEAD_DE_FC_N_05", CORE_FILESYSTEM_PATH::FindFilePath( "HEAD_DE_FC_N_05", "png", "MODEL_TEXTURES" ) );
    NakedGirlObject->GetMeshTable()[1]->SetTexture( image->CreateTextureObject( true ) );
    
    image=RESOURCE_IMAGE::LoadResourceForPath( "HEAD_DE_FC_N_05", CORE_FILESYSTEM_PATH::FindFilePath( "HEAD_DE_FC_N_05", "png", "MODEL_TEXTURES" ) );
    NakedGirlObject->GetMeshTable()[2]->SetTexture( image->CreateTextureObject( true ) );
    
    image=RESOURCE_IMAGE::LoadResourceForPath( "FEET_DE_FT_N_00", CORE_FILESYSTEM_PATH::FindFilePath( "FEET_DE_FT_N_00", "png", "MODEL_TEXTURES" ) );
    NakedGirlObject->GetMeshTable()[3]->SetTexture( image->CreateTextureObject( true ) );
    
    image=RESOURCE_IMAGE::LoadResourceForPath( "HEAD_DE_FC_N_05", CORE_FILESYSTEM_PATH::FindFilePath( "HEAD_DE_FC_N_05", "png", "MODEL_TEXTURES" ) );
    NakedGirlObject->GetMeshTable()[4]->SetTexture( image->CreateTextureObject( true ) );
    
    image=RESOURCE_IMAGE::LoadResourceForPath( "HAIR_CO_HR_N_24", CORE_FILESYSTEM_PATH::FindFilePath( "HAIR_CO_HR_N_24", "png", "MODEL_TEXTURES" ) );
    NakedGirlObject->GetMeshTable()[5]->SetTexture( image->CreateTextureObject( true ) );

    image=RESOURCE_IMAGE::LoadResourceForPath( "HANDS_DE_GL_N_00_NR", CORE_FILESYSTEM_PATH::FindFilePath( "HANDS_DE_GL_N_00_NR", "png", "MODEL_TEXTURES" ) );
    NakedGirlObject->GetMeshTable()[0]->SetNormalTexture( image->CreateTextureObject( true ) );
    
    image=RESOURCE_IMAGE::LoadResourceForPath( "HEAD_DE_FC_N_05_NR", CORE_FILESYSTEM_PATH::FindFilePath( "HEAD_DE_FC_N_05_NR", "png", "MODEL_TEXTURES" ) );
    NakedGirlObject->GetMeshTable()[1]->SetNormalTexture( image->CreateTextureObject( true ) );
    
    image=RESOURCE_IMAGE::LoadResourceForPath( "BODY_DE_BD_N_00_NR", CORE_FILESYSTEM_PATH::FindFilePath( "BODY_DE_BD_N_00_NR", "png", "MODEL_TEXTURES" ) );
    NakedGirlObject->GetMeshTable()[2]->SetNormalTexture( image->CreateTextureObject( true ) );
    
    image=RESOURCE_IMAGE::LoadResourceForPath( "FEET_DE_FT_N_00_NR", CORE_FILESYSTEM_PATH::FindFilePath( "FEET_DE_FT_N_00_NR", "png", "MODEL_TEXTURES" ) );
    NakedGirlObject->GetMeshTable()[3]->SetNormalTexture( image->CreateTextureObject( true ) );
    
    image=RESOURCE_IMAGE::LoadResourceForPath( "HEAD_DE_FC_N_05_NR", CORE_FILESYSTEM_PATH::FindFilePath( "HEAD_DE_FC_N_05_NR", "png", "MODEL_TEXTURES" ) );
    NakedGirlObject->GetMeshTable()[4]->SetNormalTexture( image->CreateTextureObject( true ) );
    
    image=RESOURCE_IMAGE::LoadResourceForPath( "HAIR_CO_HR_N_24_NR", CORE_FILESYSTEM_PATH::FindFilePath( "HAIR_CO_HR_N_24_NR", "png", "MODEL_TEXTURES" ) );
    NakedGirlObject->GetMeshTable()[5]->SetNormalTexture( image->CreateTextureObject( true ) );
    
    effect->Initialize( NakedGirlObject->GetMeshTable()[0]->GetVertexComponent() );
    effect_shadow_map->Initialize( NakedGirlObject->GetMeshTable()[0]->GetVertexComponent() );

    NakedGirlObject->SetShaderForMesh( NULL, &effect->GetProgram() );
    NakedGirlObject->SetShaderForMesh( NULL, &effect_shadow_map->GetProgram() );
    
    GRAPHIC_SHADER_EFFECT::PTR plane_shader_effect = GRAPHIC_SHADER_EFFECT::LoadResourceForPath(CORE_HELPERS_UNIQUE_IDENTIFIER( "SHADER::ShaderColor"), CORE_FILESYSTEM_PATH::FindFilePath( "ShaderColor" , "vsh", "OPENGL2" ) );
    GRAPHIC_SHADER_EFFECT::PTR line_shader_effect = GRAPHIC_SHADER_EFFECT::LoadResourceForPath(CORE_HELPERS_UNIQUE_IDENTIFIER( "SHADER::LineShader"), CORE_FILESYSTEM_PATH::FindFilePath( "LineShader" , "vsh", "OPENGL2" ) );
    GRAPHIC_SHADER_EFFECT::PTR ui_colored_shader_effect = GRAPHIC_SHADER_EFFECT::LoadResourceForPath(CORE_HELPERS_UNIQUE_IDENTIFIER( "SHADER::UIShader"), CORE_FILESYSTEM_PATH::FindFilePath( "UIShaderTextured" , "vsh", "OPENGL2" ) );
    
    SERVICE_LOGGER_Error( "ALL APP InitializeGraphics 55" );
    
    PlanObject = new GRAPHIC_OBJECT_SHAPE_PLAN;
    
    EffectPlan = new GRAPHIC_OBJECT_SHAPE_PLAN;
    CubeObject = new GRAPHIC_OBJECT_SHAPE_CUBE;
    SphereObject = new GRAPHIC_OBJECT_SHAPE_SPHERE;
    Line = new GRAPHIC_OBJECT_SHAPE_LINE;
    
    for ( int par = 0; par < NakedGirlObject->GetMeshTable().size(); par++  ) {
        
        NakedGirlObject->GetMeshTable()[ par ]->CreateBuffers();
    }
    
    /*for ( int par = 0; par < AstroBoy->GetMeshTable().size(); par++  ) {
        
        AstroBoy->GetMeshTable()[par]->CreateBuffers();
    }*/
    
    RESOURCE_IMAGE * height_map = (RESOURCE_IMAGE*) loader.Load( CORE_FILESYSTEM_PATH::FindFilePath("heightmap", "png", "MAP" ) );
                                                                 
    float * heights = (float * ) height_map->GetImageRawData();
    
    HeightMapObject = new GRAPHIC_OBJECT_SHAPE_HEIGHT_MAP( heights, height_map->GetImageInfo().Width, height_map->GetImageInfo().Height, 2.0f );
    SERVICE_LOGGER_Error( "ALL APP InitializeGraphics 56" );

    plane_shader_effect->Initialize( PlanObject->GetShaderBindParameter() );
    SERVICE_LOGGER_Error( "ALL APP InitializeGraphics 57" );
    ui_colored_shader_effect->Initialize( PlanObject->GetShaderBindParameter() );
    SERVICE_LOGGER_Error( "ALL APP InitializeGraphics 56" );
    line_shader_effect->Initialize( PlanObject->GetShaderBindParameter() );
    
    HeightMapObject->InitializeShape( &basic_geometry_effect->GetProgram(), 1 );
    HeightMapObject->SetShaderForMesh(NULL, BasicEffectShadowMap->GetProgram().GetProgram());
    CubeObject->InitializeShape( &plane_shader_effect->GetProgram() );
    CubeObject->SetShaderForMesh(NULL, &BasicEffectShadowMap->GetProgram());
    Line->InitializeShape( &line_shader_effect->GetProgram() );
    
    PlanObject->InitializeShape( &BlurEffect->GetProgram() );
    
    Moulin->SetShaderForMesh( NULL, &basic_geometry_effect->GetProgram() );
    
    CORE_MEMORY_ALLOCATOR_Free( heights );

    SERVICE_LOGGER_Error( "ALL APP InitializeGraphics 2" );
    
    #if PLATFORM_WINDOWS || PLATFORM_OSX
        ShadowMapRenderTarget.InitializeDepthTexture( 1024, 1024, GRAPHIC_TEXTURE_IMAGE_TYPE_DEPTH16 );
    
        PrimaryRenderTarget.Initialize( GetApplicationWindow().GetWidth(), GetApplicationWindow().GetHeight(), GRAPHIC_TEXTURE_IMAGE_TYPE_RGBA, true, true, 0 );
        GaussianRenderTarget.Initialize( GetApplicationWindow().GetWidth(), GetApplicationWindow().GetHeight(), GRAPHIC_TEXTURE_IMAGE_TYPE_RGBA, false, false, 0 );
        BloomRenderTarget.Initialize( GetApplicationWindow().GetWidth()/8, GetApplicationWindow().GetHeight() /8, GRAPHIC_TEXTURE_IMAGE_TYPE_RGBA, false, false, 0 );
        
        PrimaryRenderTarget.Discard();
        GaussianRenderTarget.Discard();
        BloomRenderTarget.Discard();
    
        BlurEffect = (GRAPHIC_SHADER_EFFECT_SPEEDBLUR::PTR ) GRAPHIC_SHADER_EFFECT::LoadResourceForPath(CORE_HELPERS_UNIQUE_IDENTIFIER( "SHADER::SpeedBlur"), CORE_FILESYSTEM_PATH::FindFilePath( "FullScreenSpeedBlurShader" , "", "OPENGL2" ) );
        BloomEffect = (GRAPHIC_SHADER_EFFECT_FULLSCREEN_BLOOM::PTR ) GRAPHIC_SHADER_EFFECT::LoadResourceForPath(CORE_HELPERS_UNIQUE_IDENTIFIER( "SHADER::BloomShader"), CORE_FILESYSTEM_PATH::FindFilePath( "FullscreenBloomPostProcess" , "", "OPENGL2" ) );
        HorizontalBlurEffect = (GRAPHIC_SHADER_EFFECT_FULLSCREEN_GAUSSIAN_BLUR::PTR ) GRAPHIC_SHADER_EFFECT::LoadResourceForPath(CORE_HELPERS_UNIQUE_IDENTIFIER( "SHADER::HZBlurShader"), CORE_FILESYSTEM_PATH::FindFilePath( "FullscreenGaussianHorrizontalBlurPostProcess" , "", "OPENGL2" ) );
        VerticalBlurEffect = (GRAPHIC_SHADER_EFFECT_FULLSCREEN_GAUSSIAN_BLUR::PTR ) GRAPHIC_SHADER_EFFECT::LoadResourceForPath(CORE_HELPERS_UNIQUE_IDENTIFIER( "SHADER::VBlurShader"), CORE_FILESYSTEM_PATH::FindFilePath( "FullscreenGaussianVerticalBlurPostProcess" , "", "OPENGL2" ) );
        CombineBloomEffect = (GRAPHIC_SHADER_EFFECT_FULLSCREEN_COMBINE_BLOOM::PTR ) GRAPHIC_SHADER_EFFECT::LoadResourceForPath(CORE_HELPERS_UNIQUE_IDENTIFIER( "SHADER::CombineShader"), CORE_FILESYSTEM_PATH::FindFilePath( "FullscreenCombinePostProcess" , "", "OPENGL2" ) );
    
        BlurEffect->Initialize( GRAPHIC_SHADER_BIND_PositionNormalTexture );
        BloomEffect->Initialize( GRAPHIC_SHADER_BIND_PositionNormalTexture );
        HorizontalBlurEffect->Initialize( GRAPHIC_SHADER_BIND_PositionNormalTexture );
        VerticalBlurEffect->Initialize( GRAPHIC_SHADER_BIND_PositionNormalTexture );
        CombineBloomEffect->Initialize( GRAPHIC_SHADER_BIND_PositionNormalTexture );
        
        EffectPlan->InitializeShape( &BloomEffect->GetProgram() );
        
        EffectPlan->SetEffect( BloomEffect );
        EffectPlan->SetPosition( CORE_MATH_VECTOR( 0.0f, 0.0f, 0.0f, 1.0f ) );
        EffectPlan->SetScaleFactor( CORE_MATH_VECTOR( 512.0f, 384.0f, 0.0f, 1.0f ) );
        
        TextureBlock = new GRAPHIC_TEXTURE_BLOCK;
        TextureBlock->SetOffset(CORE_MATH_VECTOR::Zero );
        TextureBlock->SetSize( CORE_MATH_VECTOR::One );
        TextureBlock->SetTexture( PrimaryRenderTarget.TargetTexture );
        
        EffectPlan->SetTextureBlock( TextureBlock );
        
        AlternateTextureBlock = new GRAPHIC_TEXTURE_BLOCK;
        AlternateTextureBlock->SetOffset(CORE_MATH_VECTOR::Zero );
        AlternateTextureBlock->SetSize( CORE_MATH_VECTOR::One );
        AlternateTextureBlock->SetTexture( GaussianRenderTarget.TargetTexture );
    
    #endif
    
    SERVICE_LOGGER_Error( "ALL APP InitializeGraphics 2.1" );
    
    BasicGeometryEffect = ( GRAPHIC_SHADER_EFFECT_SPEEDBLUR::PTR )  GRAPHIC_SHADER_EFFECT::LoadResourceForPath(CORE_HELPERS_UNIQUE_IDENTIFIER( "SHADER::ShaderPoNoUVTaBi"), CORE_FILESYSTEM_PATH::FindFilePath( "BasicGeometryShaderPoNoUVTaBi" , "vsh", "OPENGL2" ) );
    
    BasicGeometryEffect->Initialize( GRAPHIC_SHADER_BIND_PositionNormalTextureTangentBitangent );
    
    SERVICE_LOGGER_Error( "ALL APP InitializeGraphics 2.14" );
    component_entity2 = CreateMesh( CORE_FILESYSTEM_PATH::FindFilePath("T_34_85", "smx", "MODELS" ) ,&BasicGeometryEffect->GetProgram(), CORE_MATH_VECTOR( 5.0f,0.0f,.0f,0.0f ));
    SERVICE_LOGGER_Error( "ALL APP InitializeGraphics 2.14" );
    CreateMesh( CORE_FILESYSTEM_PATH::FindFilePath("thor", "smx", "MODELS" ) ,&BasicGeometryEffect->GetProgram(), CORE_MATH_VECTOR( 10.0f,0.0f,.0f,0.0f ));
    SERVICE_LOGGER_Error( "ALL APP InitializeGraphics 2.15" );
    CreateMesh( CORE_FILESYSTEM_PATH::FindFilePath("cyl", "smx", "MODELS" ) ,&BasicGeometryEffect->GetProgram(), CORE_MATH_VECTOR( 15.0f,0.0f,.0f,0.0f ));
    SERVICE_LOGGER_Error( "ALL APP InitializeGraphics 2.16" );
    CreateMesh( CORE_FILESYSTEM_PATH::FindFilePath("mesh2", "smx", "MODELS" ) ,&BasicGeometryEffect->GetProgram(), CORE_MATH_VECTOR( 20.0f,0.0f,.0f,0.0f ));
    SERVICE_LOGGER_Error( "ALL APP InitializeGraphics 2.17" );
    CreateMesh( CORE_FILESYSTEM_PATH::FindFilePath("untitled", "smx", "MODELS" ) ,&BasicGeometryEffect->GetProgram(), CORE_MATH_VECTOR( 25.0f,0.0f,.0f,0.0f ));
    SERVICE_LOGGER_Error( "ALL APP InitializeGraphics 2.18" );
    CreateMesh( CORE_FILESYSTEM_PATH::FindFilePath("Ironman", "smx", "MODELS" ) ,&BasicGeometryEffect->GetProgram(), CORE_MATH_VECTOR( 30.0f,0.0f,.0f,0.0f ));
    SERVICE_LOGGER_Error( "ALL APP InitializeGraphics 2.19" );
    RESOURCE_IMAGE::FlushCache();
    
    SERVICE_LOGGER_Error( "ALL APP InitializeGraphics 2.2" );
    
    GRAPHIC_PARTICLE_MANAGER * man = new GRAPHIC_PARTICLE_MANAGER();
    GRAPHIC_PARTICLE_EMITER<GRAPHIC_PARTICLE, GRAPHIC_PARTICLE_DYNAMIC_ATTRIBUTES, PARTICLE_SIZE> * emiter = new GRAPHIC_PARTICLE_EMITER<GRAPHIC_PARTICLE, GRAPHIC_PARTICLE_DYNAMIC_ATTRIBUTES, PARTICLE_SIZE>();
    
    GRAPHIC_MATERIAL * material = new GRAPHIC_MATERIAL( "smoke", "BasicParticleShader", GRAPHIC_SHADER_BIND_PositionNormalTexture );
    
    emiter->Initialize(200, 5.0f, material );
    
    GRAPHIC_PARTICLE_MODIFIER_APPLY_VELOCITY< GRAPHIC_PARTICLE, GRAPHIC_PARTICLE_DYNAMIC_ATTRIBUTES, PARTICLE_SIZE> * mod_v = new GRAPHIC_PARTICLE_MODIFIER_APPLY_VELOCITY< GRAPHIC_PARTICLE, GRAPHIC_PARTICLE_DYNAMIC_ATTRIBUTES, PARTICLE_SIZE>();
    
    GRAPHIC_PARTICLE_MODIFIER_GRAVITY< GRAPHIC_PARTICLE, GRAPHIC_PARTICLE_DYNAMIC_ATTRIBUTES, PARTICLE_SIZE> * mod_g = new GRAPHIC_PARTICLE_MODIFIER_GRAVITY< GRAPHIC_PARTICLE, GRAPHIC_PARTICLE_DYNAMIC_ATTRIBUTES, PARTICLE_SIZE>( 9.81f);
    
    emiter->AddModifier( *((GRAPHIC_PARTICLE_MODIFIER<GRAPHIC_PARTICLE, GRAPHIC_PARTICLE_DYNAMIC_ATTRIBUTES, PARTICLE_SIZE> *) mod_g) );
    emiter->AddModifier( *((GRAPHIC_PARTICLE_MODIFIER<GRAPHIC_PARTICLE, GRAPHIC_PARTICLE_DYNAMIC_ATTRIBUTES, PARTICLE_SIZE> *) mod_v) );
    
    man->AddEmiter(*((GRAPHIC_PARTICLE_EMITER_BASE_CLASS *)emiter));
    
    GRAPHIC_PARTICLE_SYSTEM::GetInstance().AddManager(*man);
}

void MyTestApp::CreateGround() {
    
    GAMEPLAY_COMPONENT_ENTITY * component_entity = GAMEPLAY_COMPONENT_MANAGER::GetInstance().CreateEntity();
    
    //should not be done like this
    
    component_entity->SetCompononent( GAMEPLAY_COMPONENT::FactoryCreate( GAMEPLAY_COMPONENT_TYPE_Position ), GAMEPLAY_COMPONENT_TYPE_Position );
    component_entity->SetCompononent( GAMEPLAY_COMPONENT::FactoryCreate( GAMEPLAY_COMPONENT_TYPE_Render ), GAMEPLAY_COMPONENT_TYPE_Render );
    component_entity->SetCompononent( GAMEPLAY_COMPONENT::FactoryCreate( GAMEPLAY_COMPONENT_TYPE_Physics ), GAMEPLAY_COMPONENT_TYPE_Physics );
    
    ( ( GAMEPLAY_COMPONENT_RENDER *) component_entity->GetComponent(GAMEPLAY_COMPONENT_TYPE_Render))->SetObject(  HeightMapObject );
    
    GAMEPLAY_COMPONENT_SYSTEM_RENDERER * render_system = ( GAMEPLAY_COMPONENT_SYSTEM_RENDERER * ) Scene->GetRenderableSystemTable()[0];
    
    render_system->AddEntity( component_entity );
    render_system->SetRenderer( &GRAPHIC_RENDERER::GetInstance() );
    
    GAMEPLAY_COMPONENT_SYSTEM_COLLISION_DETECTION * bullet_system = ( GAMEPLAY_COMPONENT_SYSTEM_COLLISION_DETECTION * ) Scene->GetUpdatableSystemTable()[3];
    
    CORE_MATH_VECTOR position ( -256.0f, -60.0f, -256.0f, 1.0f );
    
    ( ( GAMEPLAY_COMPONENT_PHYSICS *) component_entity->GetComponent(GAMEPLAY_COMPONENT_TYPE_Physics))->ConfigureShapePlane( position );
    
    bullet_system->AddEntity(component_entity);
    
    component_entity->SetPosition( position );
}

void MyTestApp::CreateNakedGirl() {
    
    component_entity = GAMEPLAY_COMPONENT_MANAGER::GetInstance().CreateEntity();
    component_entity->SetIndex( ComponentIndex++ );
    
    //should not be done like this
    
    component_entity->SetCompononent( GAMEPLAY_COMPONENT::FactoryCreate( GAMEPLAY_COMPONENT_TYPE_Position ), GAMEPLAY_COMPONENT_TYPE_Position );
    component_entity->SetCompononent( GAMEPLAY_COMPONENT::FactoryCreate( GAMEPLAY_COMPONENT_TYPE_Render ), GAMEPLAY_COMPONENT_TYPE_Render );
    component_entity->SetCompononent( GAMEPLAY_COMPONENT::FactoryCreate( GAMEPLAY_COMPONENT_TYPE_Physics ), GAMEPLAY_COMPONENT_TYPE_Physics );
    component_entity->SetCompononent( GAMEPLAY_COMPONENT::FactoryCreate( GAMEPLAY_COMPONENT_TYPE_Animation ), GAMEPLAY_COMPONENT_TYPE_Animation );
    
    ( ( GAMEPLAY_COMPONENT_RENDER *) component_entity->GetComponent(GAMEPLAY_COMPONENT_TYPE_Render))->SetObject( NakedGirlObject );
    
    GAMEPLAY_COMPONENT_SYSTEM_RENDERER * render_system = ( GAMEPLAY_COMPONENT_SYSTEM_RENDERER * ) Scene->GetRenderableSystemTable()[0];
    
    render_system->AddEntity( component_entity );
    render_system->SetRenderer( &GRAPHIC_RENDERER::GetInstance() );
    
    GAMEPLAY_COMPONENT_SYSTEM_COLLISION_DETECTION * bullet_system = ( GAMEPLAY_COMPONENT_SYSTEM_COLLISION_DETECTION * ) Scene->GetUpdatableSystemTable()[3];
    GAMEPLAY_COMPONENT_SYSTEM_ANIMATING * animation_system = ( GAMEPLAY_COMPONENT_SYSTEM_ANIMATING * ) Scene->GetUpdatableSystemTable()[1];
    
    CORE_MATH_VECTOR position ( 0.0f, 20.0f, 0.0f, 1.0f );
    
    ( ( GAMEPLAY_COMPONENT_PHYSICS *) component_entity->GetComponent(GAMEPLAY_COMPONENT_TYPE_Physics))->ConfigureShapeSphere( position );
    
    ( ( GAMEPLAY_COMPONENT_ANIMATION *) component_entity->GetComponent(GAMEPLAY_COMPONENT_TYPE_Animation))->SetAnimation( ((GRAPHIC_OBJECT_ANIMATED*) NakedGirlObject)->GetAnimationController() );
    
    bullet_system->AddEntity( component_entity );
    animation_system->AddEntity( component_entity );
    
    component_entity->SetPosition( position );
}

void MyTestApp::CreateMoulin() {
    
    GAMEPLAY_COMPONENT_ENTITY * component_entity = GAMEPLAY_COMPONENT_MANAGER::GetInstance().CreateEntity();
    component_entity->SetIndex( ComponentIndex++ );
    
    //should not be done like this
    
    component_entity->SetCompononent( GAMEPLAY_COMPONENT::FactoryCreate( GAMEPLAY_COMPONENT_TYPE_Position ), GAMEPLAY_COMPONENT_TYPE_Position );
    component_entity->SetCompononent( GAMEPLAY_COMPONENT::FactoryCreate( GAMEPLAY_COMPONENT_TYPE_Render ), GAMEPLAY_COMPONENT_TYPE_Render );
    component_entity->SetCompononent( GAMEPLAY_COMPONENT::FactoryCreate( GAMEPLAY_COMPONENT_TYPE_Physics ), GAMEPLAY_COMPONENT_TYPE_Physics );
    
    ( ( GAMEPLAY_COMPONENT_RENDER *) component_entity->GetComponent(GAMEPLAY_COMPONENT_TYPE_Render))->SetObject( Moulin );
    
    GAMEPLAY_COMPONENT_SYSTEM_RENDERER * render_system = ( GAMEPLAY_COMPONENT_SYSTEM_RENDERER * ) Scene->GetRenderableSystemTable()[0];
    
    render_system->AddEntity( component_entity );
    render_system->SetRenderer( &GRAPHIC_RENDERER::GetInstance() );
    
    GAMEPLAY_COMPONENT_SYSTEM_COLLISION_DETECTION * bullet_system = ( GAMEPLAY_COMPONENT_SYSTEM_COLLISION_DETECTION * ) Scene->GetUpdatableSystemTable()[3];
    
    CORE_MATH_VECTOR position ( 1.0f, 0.0f, -5000.0f, 1.0f );
    
    ( ( GAMEPLAY_COMPONENT_PHYSICS *) component_entity->GetComponent(GAMEPLAY_COMPONENT_TYPE_Physics))->ConfigureShapeSphere( position );
    
    bullet_system->AddEntity(component_entity);
    
    component_entity->SetPosition( position );
}

GAMEPLAY_COMPONENT_ENTITY * MyTestApp::CreateMesh( const CORE_FILESYSTEM_PATH & path, GRAPHIC_SHADER_PROGRAM_DATA_PROXY::PTR program, const CORE_MATH_VECTOR & position ) {
    
    GAMEPLAY_COMPONENT_ENTITY * component_entity = GAMEPLAY_COMPONENT_MANAGER::GetInstance().CreateEntity();
    component_entity->SetIndex( ComponentIndex++ );
    
    //should not be done like this
    
    GRAPHIC_OBJECT * object =GRAPHIC_MESH_MANAGER::GetInstance().LoadObject( path, 0, GRAPHIC_MESH_TYPE_ModelResource );
    
    for ( int i = 0; i < object->GetMeshTable().size(); i++ ) {
        
        object->GetMeshTable()[ i ]->CreateBuffers();
    }
    
    object->GetShaderTable().resize( 1 );
    object->GetShaderTable()[ 0 ] = program;
    
    component_entity->SetCompononent( GAMEPLAY_COMPONENT::FactoryCreate( GAMEPLAY_COMPONENT_TYPE_Position ), GAMEPLAY_COMPONENT_TYPE_Position );
    component_entity->SetCompononent( GAMEPLAY_COMPONENT::FactoryCreate( GAMEPLAY_COMPONENT_TYPE_Render ), GAMEPLAY_COMPONENT_TYPE_Render );
    component_entity->SetCompononent( GAMEPLAY_COMPONENT::FactoryCreate( GAMEPLAY_COMPONENT_TYPE_Physics ), GAMEPLAY_COMPONENT_TYPE_Physics );
    
    ( ( GAMEPLAY_COMPONENT_RENDER *) component_entity->GetComponent(GAMEPLAY_COMPONENT_TYPE_Render))->SetObject(  object );
    
    GAMEPLAY_COMPONENT_SYSTEM_RENDERER * render_system = ( GAMEPLAY_COMPONENT_SYSTEM_RENDERER * ) Scene->GetRenderableSystemTable()[0];
    
    render_system->AddEntity( component_entity );
    render_system->SetRenderer( &GRAPHIC_RENDERER::GetInstance() );
    
    GAMEPLAY_COMPONENT_SYSTEM_COLLISION_DETECTION * bullet_system = ( GAMEPLAY_COMPONENT_SYSTEM_COLLISION_DETECTION * ) Scene->GetUpdatableSystemTable()[3];
    
    ( ( GAMEPLAY_COMPONENT_PHYSICS *) component_entity->GetComponent(GAMEPLAY_COMPONENT_TYPE_Physics))->ConfigureShapePlane( position );
    
    bullet_system->AddEntity(component_entity);
    
    component_entity->SetPosition( position );
    
    return component_entity;
}

GRAPHIC_OBJECT_ANIMATED * MyTestApp::CreateAnimatedObject( const CORE_FILESYSTEM_PATH & object_path, const CORE_FILESYSTEM_PATH & animation_path ) {
    
    GRAPHIC_OBJECT_ANIMATED * animated_object = GRAPHIC_MESH_MANAGER::GetInstance().LoadObjectAnimated( object_path, 1337, GRAPHIC_MESH_TYPE_ModelResource );
    
    animated_object->SetAnimationController( new GRAPHIC_MESH_ANIMATION_CONTROLLER );
    
    for (int i = 0; i < animated_object->GetMeshTable().size(); i++ ) {
        
        char * temp_path = (char *) CORE_MEMORY_ALLOCATOR::Allocate(strlen( animation_path.GetPath() ) + 2 );
        
        strcpy(temp_path, animation_path.GetPath() );
        
        char buff[2];
        
        sprintf(buff, "%d", i);
        strcat( temp_path, buff );
        
        CORE_FILESYSTEM_PATH path( temp_path );
        
        animated_object->GetAnimationController()->Load( path );
        
        animated_object->GetAnimationController()->GetAnimation( i )->Initialize( animated_object->GetJointTable(), 0);
            
        CORE_MEMORY_ALLOCATOR::Free( temp_path );
    }
    
    animated_object->GetAnimationController()->Initialize();
    
    return animated_object;
}
