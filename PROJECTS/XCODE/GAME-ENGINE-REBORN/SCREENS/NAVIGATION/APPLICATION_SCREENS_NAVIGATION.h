//
//  APPLICATION_SCREENS_NAVIGATION.hpp
//  GAME-ENGINE-REBORN
//
//  Created by Christophe Bernard on 31/10/16.
//  Copyright © 2016 Christophe Bernard. All rights reserved.
//

#ifndef APPLICATION_SCREENS_NAVIGATION_hpp
#define APPLICATION_SCREENS_NAVIGATION_hpp

#include "CORE_HELPERS_CLASS.h"
#include "GRAPHIC_UI_FRAME.h"
#include "CORE_FIXED_STATE_MACHINE.h"
#include "CORE_HELPERS_UNIQUE.h"

class NAVIGATION_ITEM_SCREEN_PROXY_BASE {
    
    protected :
    
    virtual GRAPHIC_UI_FRAME * operator *() {
        
        return Window;
    }
    
    void ReleaseMemory() {
        
        delete Window;
        
        Window = NULL;
    }
    
    GRAPHIC_UI_FRAME * Window;
};

template <typename __SCREEN_TYPE__>
class NAVIGATION_ITEM_SCREEN_PROXY : NAVIGATION_ITEM_SCREEN_PROXY_BASE {
    
public:
    virtual GRAPHIC_UI_FRAME * operator *() {
        
        if ( Window == NULL ) {
            Window = new __SCREEN_TYPE__();
        }
        
        return Window;
    }
};

class NAVIGATION_ITEM {
    
    public :
    NAVIGATION_ITEM() : NavigationChilds() {
        
    }
    
    std::map<std::string, NAVIGATION_ITEM_SCREEN_PROXY_BASE * > GetNavigationChilds() { return NavigationChilds; }
    private :
    std::map<std::string, NAVIGATION_ITEM_SCREEN_PROXY_BASE * > NavigationChilds;
};

XS_CLASS_BEGIN( APPLICATION_SCREENS_NAVIGATION )

XS_DEFINE_UNIQUE(APPLICATION_SCREENS_NAVIGATION)

public :
    template <typename __DESTINATION_CLASS__>
    void NavigateTo(char * screenName) {
        
        NAVIGATION_ITEM * item = GetNextItemForNavigation( screenName );
        
        if ( item == NULL ) {
            item = CreateItemForNavigation<__DESTINATION_CLASS__> ( screenName );
        }
    }

    template <typename __DESTINATION_CLASS__>
    void InitializeNavigation(char * screenName) {
        
        CurrentNavigationItem = CreateItemForNavigation<__DESTINATION_CLASS__>( screenName );
    }

private :

    template <typename __DESTINATION_CLASS__>
    NAVIGATION_ITEM * CreateItemForNavigation( char * screenName ) {
        
        NAVIGATION_ITEM * item = new NAVIGATION_ITEM();
        
        std::string str(screenName);
        
        NavigationItemsTable[str] = item;
        
        return item;
    }

    NAVIGATION_ITEM * GetNextItemForNavigation( char * screenName) {
    
    return NULL;
}

NAVIGATION_ITEM *
    CurrentNavigationItem;
std::map<std::string, NAVIGATION_ITEM * >
    NavigationItemsTable;

XS_CLASS_END

#endif /* APPLICATION_SCREENS_NAVIGATION_hpp */
