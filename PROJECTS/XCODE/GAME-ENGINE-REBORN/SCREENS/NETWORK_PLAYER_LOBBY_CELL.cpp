//
//  NETWORK_PLAYER_LOBBY_CELL.cpp
//  GAME-ENGINE-REBORN
//
//  Created by Christophe Bernard on 27/11/16.
//  Copyright © 2016 Christophe Bernard. All rights reserved.
//

#include "NETWORK_PLAYER_LOBBY_CELL.h"

NETWORK_PLAYER_LOBBY_CELL::NETWORK_PLAYER_LOBBY_CELL() {
    
}

NETWORK_PLAYER_LOBBY_CELL::~NETWORK_PLAYER_LOBBY_CELL() {
    
}

void NETWORK_PLAYER_LOBBY_CELL::Initialize() {
    
    GRAPHIC_UI_RENDER_STYLE * default_render_style = new GRAPHIC_UI_RENDER_STYLE;
    
    default_render_style->SetColor( CORE_MATH_VECTOR( 1.0f, 0.0f, 1.0f, 1.0f ) );
    default_render_style->SetShape( Shape );
    default_render_style->SetTextureBlock( &TextureAtlas.GetTextureBlock( CORE_HELPERS_UNIQUE_IDENTIFIER( "Create_Server_button" ) ) );
    
    GRAPHIC_UI_ELEMENT * stop_lobby_button = new GRAPHIC_UI_ELEMENT;
    
    stop_lobby_button->SetRenderStyleForState( GRAPHIC_UI_ELEMENT_STATE_Default, default_render_style );
    stop_lobby_button->SetActionCallback( *StopLobbyButtonClickedCallback );
    stop_lobby_button->GetPlacement().Initialize( &GetPlacement(),
                                                 CORE_MATH_VECTOR::Zero,
                                                 CORE_MATH_VECTOR( 128.0f, 128.0f, 0.0f, 1.0f ),
                                                 GRAPHIC_UI_BottomLeft );
    
}
