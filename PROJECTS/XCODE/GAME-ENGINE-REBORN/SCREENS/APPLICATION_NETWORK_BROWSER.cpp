//
//  APPLICATION_NETWORK_BROWSER.cpp
//  GAME-ENGINE-REBORN
//
//  Created by Christophe Bernard on 30/10/16.
//  Copyright © 2016 Christophe Bernard. All rights reserved.
//

#include "APPLICATION_NETWORK_BROWSER.h"
#include "GRAPHIC_UI_FRAME_LIST_ADAPTER.h"
#include "NETWORK_PLAYER_LOBBY_CELL.h"

APPLICATION_NETWORK_BROWSER::APPLICATION_NETWORK_BROWSER() :
    ServersList(),
    MessageReceivedCount(0),
    LastMessage( NULL ) {
    
}

APPLICATION_NETWORK_BROWSER::~APPLICATION_NETWORK_BROWSER() {
    
}

void APPLICATION_NETWORK_BROWSER::Initialize() {
    
    GRAPHIC_UI_FRAME * item_template = (GRAPHIC_UI_FRAME*) new NETWORK_PLAYER_LOBBY_CELL();
    
    GRAPHIC_UI_FRAME * list_frame = new GRAPHIC_UI_FRAME;
    
    list_frame->SetAdapter(new GRAPHIC_UI_FRAME_LIST_ADAPTER(item_template));
    
    ElementTable[ IdServerList ] = list_frame;
}

void APPLICATION_NETWORK_BROWSER::Finalize() {
    
}

void APPLICATION_NETWORK_BROWSER::OnShow() {
    
}

void APPLICATION_NETWORK_BROWSER::OnHide() {
    
}

void APPLICATION_NETWORK_BROWSER::SetServer( SERVICE_NETWORK_CONNECTION * server_connection ) {
    
    std::vector< APPLICATION_NETWORK_REMOTE_SERVER_INFO * >::iterator it;
    
    it = ServersList.begin();
    
    while ( it != ServersList.end() ) {
        
        if ( (*it)->ServerConnexion == server_connection )
        {
            //Update server info
        }
        else {
            
            APPLICATION_NETWORK_REMOTE_SERVER_INFO * info = new APPLICATION_NETWORK_REMOTE_SERVER_INFO();
            //info->Info = server_connection;
            info->ServerConnexion = server_connection;
            
            ServersList.push_back( info );
            
            break;
        }
        
        it++;
    }
}

void APPLICATION_NETWORK_BROWSER::ConnectToServer( APPLICATION_NETWORK_REMOTE_SERVER_INFO * ) {
    
}

void APPLICATION_NETWORK_BROWSER::StartServer() {
    
}

CORE_HELPERS_IDENTIFIER
    APPLICATION_NETWORK_BROWSER::IdServerList( "NB:ServerList" );
