//
//  NETWORK_PLAYER_LOBBY_CELL.hpp
//  GAME-ENGINE-REBORN
//
//  Created by Christophe Bernard on 27/11/16.
//  Copyright © 2016 Christophe Bernard. All rights reserved.
//

#ifndef NETWORK_PLAYER_LOBBY_CELL_hpp
#define NETWORK_PLAYER_LOBBY_CELL_hpp

#include "CORE_HELPERS_CLASS.h"
#include "GRAPHIC_UI_FRAME.h"

XS_CLASS_BEGIN_WITH_ANCESTOR( NETWORK_PLAYER_LOBBY_CELL, GRAPHIC_UI_FRAME )

NETWORK_PLAYER_LOBBY_CELL();

virtual void Initialize();

private:



XS_CLASS_END

#endif /* NETWORK_PLAYER_LOBBY_CELL_hpp */
