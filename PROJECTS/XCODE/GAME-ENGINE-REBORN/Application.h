//
//  Application.h
//  GAME-ENGINE-REBORN
//
//  Created by Christophe Bernard on 1/12/13.
//  Copyright (c) 2013 Christophe Bernard. All rights reserved.
//

#ifndef __GAME_ENGINE_REBORN__Application__
#define __GAME_ENGINE_REBORN__Application__

#include "CORE_APPLICATION.h"
#include "GRAPHIC_RENDERER.h"
#include "GRAPHIC_OBJECT.h"
#include "GRAPHIC_MESH.h"
#include "GRAPHIC_MESH_MANAGER.h"
#include "GRAPHIC_OBJECT_SHAPE_PLAN.h"
#include "GRAPHIC_OBJECT_SHAPE_CUBE.h"
#include "GRAPHIC_OBJECT_SHAPE_HEIGHT_MAP.h"
#include "GRAPHIC_OBJECT_SHAPE_SPHERE.h"
#include "GRAPHIC_OBJECT_SHAPE_LINE.h"
#include "GAMEPLAY_SCENE.h"
#include "CORE_MATH_VECTOR.h"
#include "APPLICATION_MAIN_WINDOW.h"
#include "GRAPHIC_CAMERA_ORTHOGONAL.h"
#include "CORE_FILESYSTEM.h"
#include "CORE_MEMORY.h"
#include "CORE_PARALLEL_THREAD.h"
#include "SERVICE_NETWORK_SYSTEM.h"
#include "SERVICE_NETWORK_LOBBY.h"
#include "SERVICE_NETWORK_SYSTEM.h"
#include "GRAPHIC_RENDER_TARGET.h"
#include "GRAPHIC_SHADER_LIGHT.h"
#include "GRAPHIC_SHADER_EFFECT_SPEEDBLUR.h"
#include "GRAPHIC_SHADER_EFFECT_FULLSCREEN_BLOOM.h"
#include "GRAPHIC_SHADER_EFFECT_FULLSCREEN_COMBINE_BLOOM.h"
#include "GRAPHIC_SHADER_EFFECT_FULLSCREEN_GAUSSIAN_BLUR.h"
#include "GRAPHIC_OBJECT_ANIMATED.h"
#include "CORE_FIXED_STATE_MACHINE.h"
#include "GRAPHIC_PARTICLE_SYSTEM.h"
#include "GRAPHIC_PARTICLE_MODIFIER_APPLY_VELOCITY.h"
#include "GRAPHIC_PARTICLE_MODIFIER_GRAVITY.h"
#include "NETWORK_SERVER.h"
#include "NETWORK_CLIENT.h"
#include "APPLICATION_COMMAND_MANAGER.h"
#include "GLOBAL_RESOURCES.h"

#include "ScriptEntity.h"
#include "ApplicationSoundBank.h"
#include "APPLICATION_COMMAND.h"

#if GRAPHIC_RENDERER_OPENGL
    #define GRAPHIC_RENDER_PLATFORM OPENGL
        #include "GRAPHIC_RENDERER.h"
    #undef GRAPHIC_RENDER_PLATFORM
#endif

#if GRAPHIC_RENDERER_DX_9
    #define GRAPHIC_RENDER_PLATFORM DX_9
        #include "GRAPHIC_RENDERER.h"
    #undef GRAPHIC_RENDER_PLATFORM
#endif

#if GRAPHIC_RENDERER_DX_11
    #define GRAPHIC_RENDER_PLATFORM DX_11
        #include "GRAPHIC_RENDERER.h"
    #undef GRAPHIC_RENDER_PLATFORM
#endif

XS_CLASS_BEGIN_WITH_ANCESTOR( MyTestApp, CORE_APPLICATION )

    CORE_ABSTRACT_PROGRAM_DECLARE_CLASS( MyTestApp );

    //TODO Define inside state machine
    CORE_FIXED_STATE_MACHINE_DefineEvent( UPDATE_EVENT, float )
    CORE_FIXED_STATE_MACHINE_DefineEventVoid( GOTO_LOBBY )

    CORE_FIXED_STATE_MACHINE_DeclareBaseState( X_BASE_STATE, MyTestApp )
        CORE_FIXED_STATE_MACHINE_DeclareHandleEvent( UPDATE_EVENT )
    CORE_FIXED_STATE_MACHINE_End()

    CORE_FIXED_STATE_MACHINE_DefineState( X_BASE_STATE, INITIAL_STATE )
        CORE_FIXED_STATE_MACHINE_DefineHandleEvent( UPDATE_EVENT )
    CORE_FIXED_STATE_MACHINE_EndDefineState( INITIAL_STATE )

    CORE_FIXED_STATE_MACHINE_DefineState( X_BASE_STATE, LOADING_STATE )
        CORE_FIXED_STATE_MACHINE_DefineHandleEvent( UPDATE_EVENT )
    CORE_FIXED_STATE_MACHINE_EndDefineState( LOADING_STATE )

    CORE_FIXED_STATE_MACHINE_DefineState( X_BASE_STATE, MENU_STATE );
        CORE_FIXED_STATE_MACHINE_DefineHandleEvent( UPDATE_EVENT )
        CORE_FIXED_STATE_MACHINE_DefineHandleEvent( GOTO_LOBBY )
    CORE_FIXED_STATE_MACHINE_EndDefineState( MENU_STATE )

    CORE_FIXED_STATE_MACHINE_DefineState( X_BASE_STATE, GAME_STATE )
        CORE_FIXED_STATE_MACHINE_DefineHandleEvent( UPDATE_EVENT );
    CORE_FIXED_STATE_MACHINE_EndDefineState( GAME_STATE )

    CORE_FIXED_STATE_MACHINE_DefineState( GAME_STATE, PAUSE_STATE )
        CORE_FIXED_STATE_MACHINE_DefineHandleEvent( UPDATE_EVENT )
    CORE_FIXED_STATE_MACHINE_EndDefineState( PAUSE_STATE )

    MyTestApp();

    virtual void Initialize();
    virtual void Finalize();

    virtual void Update( float time_step );
    virtual void Render();

    void SetDisplacement( float x, float y, float z ) {
        
        Displacement[0] = x;
        Displacement[1] = y;
        Displacement[2] = z;
    }

    void InitializeGraphics();

    void OnObjectPicked( GAMEPLAY_COMPONENT_ENTITY * entity );

    GAMEPLAY_SCENE * GetScene() { return Scene; }

    CORE_FILESYSTEM & GetDefaultFileystem() { return DefaultFileystem; }

    CORE_FIXED_STATE_MACHINE< X_BASE_STATE, MyTestApp > & GetStateMachine() { return StateMachine; }

    GRAPHIC_RENDER_TARGET & GetShadowMapRenderTarget() { return ShadowMapRenderTarget;}

    NETWORK_SERVER & GetServer() { return Server; }
    NETWORK_CLIENT & GetClient() { return Client; }

    void SetIsClient( bool is_client ) { IsClient = is_client; }

private :

    void CreateGround();
    void CreateNakedGirl();
    void CreateMoulin();
    GAMEPLAY_COMPONENT_ENTITY * CreateMesh( const CORE_FILESYSTEM_PATH & path, GRAPHIC_SHADER_PROGRAM_DATA_PROXY::PTR program, const CORE_MATH_VECTOR & position );
    GRAPHIC_OBJECT_ANIMATED * CreateAnimatedObject( const CORE_FILESYSTEM_PATH & object_path, const CORE_FILESYSTEM_PATH & animation_path );
    GRAPHIC_OBJECT
        * NakedGirlObject,
        * AstroBoy,
        * Moulin,
        * ResourceObject;
    GRAPHIC_OBJECT_SHAPE_PLAN
        * PlanObject,
        * EffectPlan;
    GAMEPLAY_COMPONENT_ENTITY * component_entity, *component_entity2;
    GRAPHIC_OBJECT_SHAPE_CUBE * CubeObject;
    GRAPHIC_OBJECT_SHAPE_HEIGHT_MAP * HeightMapObject;
    GRAPHIC_OBJECT_SHAPE_SPHERE * SphereObject;
    GAMEPLAY_SCENE * Scene;
    CORE_MATH_VECTOR Position;
    CORE_MATH_QUATERNION Lookat;
    GRAPHIC_CAMERA * Camera, * LightCamera;
    GRAPHIC_OBJECT_SHAPE_LINE * Line;
    APPLICATION_MAIN_WINDOW MainWindow;
    GRAPHIC_CAMERA_ORTHOGONAL
        * InterfaceCamera,
        * RenderTargetCamera;
    CORE_FILESYSTEM DefaultFileystem;
    CORE_MATH_VECTOR Displacement;
    GRAPHIC_RENDER_TARGET
        ShadowMapRenderTarget,
        PrimaryRenderTarget,
        LightRenderTarget,
        SpecularRenderTarget,
        GaussianRenderTarget,
        BloomRenderTarget;
    GRAPHIC_SHADER_EFFECT
        ShadowMapEffect;
    GRAPHIC_SHADER_EFFECT::PTR
        BasicEffectShadowMap;
    GRAPHIC_SHADER_EFFECT_SPEEDBLUR::PTR
        BlurEffect,
        BasicGeometryEffect;
    GRAPHIC_SHADER_EFFECT_FULLSCREEN_BLOOM::PTR
        BloomEffect;
    GRAPHIC_SHADER_EFFECT_FULLSCREEN_GAUSSIAN_BLUR::PTR
        HorizontalBlurEffect,
        VerticalBlurEffect;
    GRAPHIC_SHADER_EFFECT_FULLSCREEN_COMBINE_BLOOM::PTR
        CombineBloomEffect;
    GRAPHIC_SHADER_LIGHT
        * DirectionalLight,
        * PointLightOne,
        * PointLightTwo,
        * SpotLightOne,
        * SpotLightTwo;
    CORE_MATH_VECTOR
        PreviousPosition;
    GRAPHIC_TEXTURE_BLOCK
        * TextureBlock,
        * AlternateTextureBlock;
    int
        BoneLevel,
        ComponentIndex;
    ScriptEntity
        TestEntity;
    CORE_ABSTRACT_PROGRAM_FACTORY
        * LuaScript;
    CORE_FIXED_STATE_MACHINE< X_BASE_STATE, MyTestApp >
        StateMachine;
    NETWORK_SERVER
        Server;
    NETWORK_CLIENT
        Client;
    bool
        IsClient;

XS_CLASS_END

#endif /* defined(__GAME_ENGINE_REBORN__Application__) */
