//
//  GLOBAL_RESOURCES.cpp
//  GAME-ENGINE-REBORN
//
//  Created by Christophe Bernard on 4/12/16.
//  Copyright © 2016 Christophe Bernard. All rights reserved.
//

#include "GLOBAL_RESOURCES.h"

void GLOBAL_RESOURCES::Initialize() {
    
    UITextureAtlas.Load(
        CORE_FILESYSTEM_PATH::FindFilePath( "atlas_test", "iax", "IMAGES" ),
        CORE_FILESYSTEM_PATH::FindFilePath( "atlas_test", "png", "IMAGES" ));
    
    UIPlanObject = new GRAPHIC_OBJECT_SHAPE_PLAN;
    
    UIPlanObject->InitializeShape( &ui_colored_shader_effect->GetProgram() );
}

void GLOBAL_RESOURCES::Finalize() {
 
    UIPlanObject->Release();
    
    UITextureAtlas.Finalize();
    
    CORE_MEMORY_ObjectSafeDeallocation( UIPlanObject );
}
